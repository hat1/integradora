/*-------------------MASCOTAS-----------------------*/
/*ESTE ES EL BUENO XD*/
DROP VIEW IF EXISTS vMascota;
CREATE VIEW vMascota AS
SELECT m.idMascota, m.nombre as nombreM, m.edad, m.raza, m.especie, m.sexo, m.descripcion, m.foto ,m.estatus, c.idCliente, p.idPersona, p.nombre, p.apellidoP, p.apellidoM FROM mascota m 
INNER JOIN cliente c ON  m.idCliente = c.idCliente
INNER JOIN persona p ON c.idPersona = p.idPersona ;            

SELECT * FROM vMascota;

SELECT * FROM vMascota WHERE estatus = 1 AND idCliente = 4 ORDER BY nombreM;

/*-------------------CITAS-------------------------*/
DROP VIEW IF EXISTS vCitas;
CREATE VIEW vCitas AS
	SELECT 
		C.idCita, C.fechaIni, C.fechaFin, C.estatus AS "estatus_Cita", 
		M.idMascota, M.nombre AS "nombre_Mascota", M.edad, M.raza, M.especie, M.sexo, M.descripcion, M.estatus AS "estatus_Mascota",
		Cl.idCliente, Cl.correo, Cl.estatus AS "estatus_Cliente",
		U.nombreUsuario, U.rol,
		P.idPersona,P.nombre AS "nombre_Persona",P.apellidoP, P.apellidoM, P.genero, P.calle, P.colonia, P.cp, P.telefono 
	FROM 
		persona P 
			INNER JOIN cliente Cl ON Cl.idPersona = P.idPersona
			INNER JOIN usuario U ON Cl.idUsuario = U.idUsuario
			INNER JOIN citas C ON C.idCliente = Cl.idCliente
			INNER JOIN mascota M ON C.idMascota = M.idMascota;
            
/*----------------------  CLIENTE  --------------------------*/
DROP VIEW IF EXISTS vCliente;
CREATE VIEW vCliente AS
SELECT c.idCliente, p.idPersona, u.idUsuario, p.nombre, p.apellidoP, p.apellidoM, p.genero, p.telefono, p.calle,p.colonia, p.cp, u.nombreUsuario, c.correo, c.estatus, u.rol FROM cliente c
INNER JOIN persona p ON p.idPersona = c.idPersona
INNER JOIN usuario u ON u.idUsuario = c.idUsuario;
SELECT * FROM vCliente where estatus = 1 order by nombre;
SELECT * FROM vCliente where estatus = 0 order by nombre;


/*-------------------  Producto  -----------------------*/
DROP VIEW IF EXISTS vProducto;
CREATE VIEW vProducto AS
SELECT pr.idProducto, pr.nombrepr, pr.descripccion, pr.precio, pr.proveedor, pr.foto, pr.estatus FROM producto pr;

select * from vProducto where estatus = 1 order by nombrepr;


/*-------------------  EMPLEADO  -----------------------*/
DROP VIEW IF EXISTS vEmpleado;
CREATE VIEW vEmpleado AS
SELECT e.idEmpleado, e.puesto,  p.idPersona, p.nombre, p.apellidoP, p.apellidoM, u.idUsuario, u.rol FROM empleado e 
INNER JOIN usuario u ON u.idUsuario = e.idUsuario
inner join persona p on p.idPersona = e.idPersona;

select * from vEmpleado order by nombre;

/*------------------SERVICIO -------------------------*/
DROP VIEW IF EXISTS vServicio;
CREATE VIEW vServicio AS
	SELECT idServicio, nombreServ, descripcionServ, precio, tipo, estatus FROM servicio;
    
SELECT * FROM vServicio WHERE estatus = 1;
/*----------------CITAS-------------------------------*/

DROP VIEW IF EXISTS vCitas;
CREATE VIEW vCitas AS
	SELECT 
		C.idCita, C.fechaIni, C.fechaFin, C.estatus AS "estatus_Cita", 
		M.idMascota, M.nombre AS "nombre_Mascota", M.edad, M.raza, M.especie, M.sexo, M.foto, M.descripcion, M.estatus AS "estatus_Mascota",
		Cl.idCliente, Cl.correo, Cl.estatus AS "estatus_Cliente",
		U.nombreUsuario, U.rol,
		P.idPersona,P.nombre AS "nombre_Persona",P.apellidoP, P.apellidoM, P.genero, P.calle, P.colonia, P.cp, P.telefono,
        S.idServicio, S.nombreServ, S.descripcionServ, S.precio
	FROM 
		persona p
			INNER JOIN cliente Cl ON Cl.idPersona = P.idPersona
			INNER JOIN usuario U ON Cl.idUsuario = U.idUsuario
			INNER JOIN citas C ON C.idCliente = Cl.idCliente
            INNER JOIN mascota M ON C.idMascota = M.idMascota
            INNER JOIN servicio S ON S.idServicio = C.idServicio;
            
select * from vCitas;

select * from vCitas where estatus_Cita= 1 ORDER BY nombre_Mascota;

/*-------------------LOGIN----------------------------*/
DROP VIEW IF EXISTS vClienteLog;
CREATE VIEW vClienteLog AS
	SELECT * FROM cliente;

DROP VIEW IF EXISTS vUsuLog;
CREATE VIEW vUsuLog AS
	SELECT idUsuario, nombreUsuario, contrasena FROM usuario;
    
SELECT * FROM vUsuLog;
    
-- SELECT * FROM cliente WHERE idUsuario IN (SELECT idUsuario FROM usuario WHERE nombreUsuario = "ana_k_p" AND contrasena = "asdf") AND estatus = 1;
/*
Se usan dos views para la parte del login, esta funciona con una subconsulta, primero buscando el idUsuario
con el nombreUsuario y contrasena ingresados, luego compara la id resultante con la id del comando WHERE
de la consulta principal.
*/
SELECT * FROM vClienteLog WHERE idUsuario IN (SELECT idUsuario FROM vUsuLog WHERE nombreUsuario = "admin" AND contrasena = "admin") AND estatus = 1;