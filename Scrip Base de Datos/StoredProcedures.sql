/*--------------------PRODUCTO-----------------------------*/
-- INSERTAR --
DROP PROCEDURE IF EXISTS insertProducto;
DELIMITER $$
CREATE PROCEDURE insertProducto( 
    nom VARCHAR(64),
    des	VARCHAR(500),
    pre	double,
    pro	VARCHAR(64),
    foto LONGTEXT
    )                               
BEGIN
    #1. Insertamos en producto:
INSERT INTO producto (nombrepr, descripccion, precio, proveedor, foto)
			VALUES(nom, des, pre, pro, foto);     
END
$$


-- MODIFICAR --
DROP PROCEDURE IF EXISTS updateProducto;
DELIMITER $$
CREATE PROCEDURE updateProducto
(	idPr INT,
    nom VARCHAR(45),
    des VARCHAR(500),
    pre DOUBLE,
    pro VARCHAR(45),
    img longtext
)     
BEGIN
	UPDATE producto
    SET nombrepr = nom,
    descripccion = des,
    precio = pre,
    proveedor = pro,
    foto = img
    WHERE idProducto = idPr;
END
$$



-- ELIMINAR --
-- DESACTIVAR PRODUCTO --
DROP PROCEDURE IF EXISTS deleteProducto;
DELIMITER $$
CREATE PROCEDURE deleteProducto(
	IN idPr INT
)
BEGIN
	UPDATE producto
    SET estatus = 0
    WHERE idProducto = idPr;
END
$$


-- ACTIVAR PRODUCTO --
DELIMITER $$
CREATE PROCEDURE actiProduc (
	IN idProc INT
)
BEGIN
	UPDATE producto
    SET estatus = 1
    WHERE idProducto = idProduc;
END
$$


/*------------------------------CITAS----------------------------------*/


/*INSERT*/
DROP PROCEDURE IF EXISTS insertCita;
DELIMITER $$
CREATE PROCEDURE insertCita(
out idC int,
in idM int,
in idCli int,
in idS int,
in fechaIni varchar(20))
BEGIN
DECLARE fi datetime;
DECLARE ff datetime;
set fi= STR_TO_DATE(fechaIni, '%d/%m/%Y %H:%i:%s');
set ff = date_add(fi,INTERVAL 90 MINUTE);
insert into citas(idCita,idMascota,idCliente,idServicio,fechaIni,fechaFin) values(idC,idM,idCli,idS,fi,ff);
SET idC = last_insert_id();
END
$$
DELIMITER ;

/*UPDATE EMPLEADO*/
DROP PROCEDURE IF EXISTS updateCitaEmpleado;
DELIMITER $$
CREATE PROCEDURE updateCitaEmpleado (
    IN fechaI VARCHAR(20),
    IN idMasc INT, 
    IN idServ INT,
    IN idCit INT)
BEGIN
DECLARE fi datetime;
DECLARE ff datetime;
set fi= STR_TO_DATE(fechaI, '%d/%m/%Y %H:%i:%s');
set ff = date_add(fi,INTERVAL 90 MINUTE);
	UPDATE citas SET
		fechaIni = fi,
        fechaFin = ff,
		idMascota = idMasc,
		idServicio = idServ
	WHERE idCita = idCit;
END
$$

/*UPDATE CLIENTE*/
DROP PROCEDURE IF EXISTS updateCitaCliente;
DELIMITER $$
CREATE PROCEDURE updateCitaCliente (
    IN fechaI VARCHAR(20),
    IN idMasc INT, 
    IN idServ INT,
    IN idCit INT)
BEGIN
DECLARE fi datetime;
DECLARE ff datetime;
set fi= STR_TO_DATE(fechaI, '%d/%m/%Y %H:%i:%s');
	UPDATE citas SET
		fechaIni = fi,
        fechaFin = ff,
		idMascota = idMasc,
		idServicio = idServ
	WHERE idCita = idCit;
END
$$


/*DELETE*/
DROP PROCEDURE IF EXISTS cambiarEstado;
DELIMITER $$
CREATE PROCEDURE cambiarEstado(
in idC int,
in est INT)
BEGIN
UPDATE citas set
estatus =  est
where idCita = idC;
end 
$$
DELIMITER ;


/*----------------------------MASCOTAS---------------------------------*/
/*
Se agrego el apartado de foto a la tabla mascota, esto genera cambios
en el storeProcedure realicionados a esta tabla.
*/
DROP PROCEDURE IF EXISTS insertMascota;
DELIMITER $$
CREATE PROCEDURE insertMascota( 
    IN _nombre VARCHAR(30),
    IN _edad VARCHAR(10),
    IN _raza VARCHAR(30),
    IN _especie VARCHAR(30),
    IN _sexo VARCHAR(6),
    IN _descripcion VARCHAR(300),
    in _foto LONGTEXT,
    IN _idCliente INT
    )                               
BEGIN
INSERT INTO mascota (nombre, edad, raza, especie, sexo, descripcion, idCliente, foto)
VALUES(_nombre, _edad, _raza, _especie, _sexo, _descripcion, _idCliente, _foto);
END
$$
DELIMITER ;

DROP PROCEDURE IF EXISTS updateMascota;
DELIMITER $$
CREATE PROCEDURE updateMascota(
	IN _idMascota INT, 
	IN _nombre VARCHAR(30),
    IN _edad VARCHAR(10),
    IN _raza VARCHAR(30),
    IN _especie VARCHAR(30),
    IN _sexo VARCHAR(6),
    IN _descripcion VARCHAR(300),
    IN _foto LONGTEXT,
    IN _idCliente INT
)
BEGIN
    UPDATE mascota
    SET idMascota = _idMascota, nombre = _nombre, edad = _edad, raza = _raza, especie = _especie, sexo = _sexo, descripcion = _descripcion, foto = _foto, idCliente = _idCliente, estatus = 1		
    WHERE idMascota = _idMascota;
END
$$

DROP PROCEDURE IF EXISTS deleteMascota;
DELIMITER $$
CREATE PROCEDURE deleteMascota(
	IN _idMascota INT
)
BEGIN
    UPDATE mascota
    SET estatus = 0 
    WHERE idMascota = _idMascota;
    
END
$$ 
DELIMITER ;

/*----------------------------SERVICIOS---------------------------------*/
-- INSERT --
DELIMITER $$
CREATE PROCEDURE insertServicio (
	IN nom VARCHAR(30),
    IN descSrv VARCHAR(400),
    IN pre DOUBLE,
    IN typS TINYINT
)
BEGIN
	INSERT INTO servicio (nombreServ, descripcionServ, precio, tipo) VALUES (nom, descSrv, pre, typs);
END
$$

-- MODIFICAR --
DELIMITER $$
CREATE PROCEDURE updateServicio (
	IN nom VARCHAR(30),
    IN descSrv VARCHAR(400),
    IN pre DOUBLE,
    IN typS TINYINT,
    IN idS INT
)
BEGIN
	UPDATE servicio SET
		nombreServ = nom,
        descripcionServ = descSrv,
        precio = pre,
        tipo = typS
	WHERE idServicio = idS;
END
$$

-- ELIMINAR --
DELIMITER $$
CREATE PROCEDURE deleteServicio (
    IN idS INT
)
BEGIN
	UPDATE servicio SET
		estatus = 0
	WHERE idServicio = idS;
END
$$

-- ACTIVAR --
DELIMITER $$
CREATE PROCEDURE actiServicio (
    IN idS INT
)
BEGIN
	UPDATE servicio SET
		estatus = 1
	WHERE idServicio = idS;
END
$$

/*----------------  CLIENTES  --------------*/
/*
Se agrego el campo idC de salida, esto para devolverl el id del cliente
creado. Este resultado se empleara en la aplicacion movil
*/
DELIMITER $$
CREATE PROCEDURE insertCliente (
	IN mail VARCHAR(200),
    IN nom VARCHAR(64),
    IN aP varchar(64),
    IN aM varchar(64),
    IN gen varchar(1),
    in cal varchar(64),
    in col varchar(64),
    in cp varchar(11),
    in tel varchar(15),
    in nomUsuario varchar(48),
    in pass varchar(48),
    out idP int,
    out idU int,
    out idC int
)
BEGIN
	INSERT INTO persona (nombre, apellidoP, apellidoM, genero, calle, colonia, cp, telefono)
    VALUES (nom, aP, aM, gen, cal, col, cp,tel);
    SET idP = Last_insert_id();
    
    INSERT INTO usuario (nombreUsuario, contrasena, rol) 
    VALUES (nomUsuario,pass,'cliente');
    SET idU = Last_insert_id();
    
    INSERT INTO cliente (correo, estatus, idPersona, idUsuario)
    VALUES (mail,1, idP, idU);
    SET idC = last_insert_id();
END
$$


-- MODIFICAR --
DELIMITER $$
CREATE PROCEDURE updateCliente (
	IN mail VARCHAR(200),
    IN nom VARCHAR(64),
    IN aP varchar(64),
    IN aM varchar(64),
    IN gen varchar(1),
    in cal varchar(64),
    in col varchar(64),
    in cp varchar(11),
    in tel varchar(15),
    in nomUsuario varchar(24),
    IN idC INT,
    IN idP INT,
    IN idU INT
)
BEGIN
	UPDATE cliente SET
		correo = mail
	WHERE idCliente = idc;
    UPDATE persona SET
		nombre = nom,
        apellidoP = aP,
        apellidoM = aM,
        genero = gen,
        calle = cal,
        colonia = col,
        cp = cp,
        telefono = tel
	WHERE idPersona = idP;
    UPDATE usuario SET
		nombreUsuario = nomUsuario
    WHERE idUsuario = idU;
END
$$


-- ELIMINAR --
DELIMITER $$
CREATE PROCEDURE deleteCliente (
    IN idC INT
)
BEGIN
	UPDATE cliente SET
		estatus = 0
	WHERE idCliente = idC;
END
$$



-- ACTIVAR --
DELIMITER $$
CREATE PROCEDURE actiCliente (
    IN idC INT
)
BEGIN
	UPDATE cliente SET
		estatus = 1
	WHERE idCliente = idC;
END
$$


