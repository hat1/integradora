/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.utl.dsm501.happypet.model;

/**
 *
 * @author PC
 */
public class Producto {

    int idProducto;
    String nombrepr;
    String descripccion;
    double precio;
    String proveedor;
    String foto;
    boolean estatus;
    

    public Producto() {
    }

    public Producto(int idProducto, String nombrepr, String descripccion, double precio, String proveedor, String foto, boolean estatus) {
        this.idProducto = idProducto;
        this.nombrepr = nombrepr;
        this.descripccion = descripccion;
        this.precio = precio;
        this.proveedor = proveedor;
        this.foto = foto;
        this.estatus = estatus;
        
    }

    public int getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(int idProducto) {
        this.idProducto = idProducto;
    }

    public String getNombrepr() {
        return nombrepr;
    }

    public void setNombrepr(String nombrepr) {
        this.nombrepr = nombrepr;
    }

    public String getDescripccion() {
        return descripccion;
    }

    public void setDescripccion(String descripccion) {
        this.descripccion = descripccion;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public String getProveedor() {
        return proveedor;
    }

    public void setProveedor(String proveedor) {
        this.proveedor = proveedor;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public boolean isEstatus() {
        return estatus;
    }

    public void setEstatus(boolean estatus) {
        this.estatus = estatus;
    }

    

    @Override
    public String toString() {
        return "Producto{" + "idProducto=" + idProducto + ", nombrepr=" + nombrepr + ", descripccion=" + descripccion +
                ", precio=" + precio + ", proveedor=" + proveedor + ", foto=" + foto + ", estatus=" + estatus + '}';
    }

    
    
}
