/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.utl.dsm501.happypet.model;

/**
 *
 * @author Jonathan Ulises
 */
public class Mascota {
    
    private int idMascota;
    private String nombre;
    private String edad;
    private String raza;
    private String especie;
    private String sexo;
    private String descripcion;
    private String foto;
    private int estatus;
    private Cliente cliente;

    public Mascota() {
    }

    public Mascota(int idMascota) {
        this.idMascota = idMascota;
    }

    public Mascota(int idMascota, String nombre, String edad, String raza, String especie, String sexo, String descripcion,String foto, int estatus, Cliente cliente) {
        this.idMascota = idMascota;
        this.nombre = nombre;
        this.edad = edad;
        this.raza = raza;
        this.especie = especie;
        this.sexo = sexo;
        this.descripcion = descripcion;
        this.foto = foto;
        this.estatus = estatus;
        this.cliente = cliente;
    }

    public Mascota(int idMascota, String nombre, String edad, String raza, String especie, String sexo, String descripcion, String foto, Cliente cliente) {
        this.idMascota = idMascota;
        this.nombre = nombre;
        this.edad = edad;
        this.raza = raza;
        this.especie = especie;
        this.sexo = sexo;
        this.descripcion = descripcion;
        this.foto = foto;
        this.cliente = cliente;
    }
    

    public Mascota(String nombre, String edad, String raza, String especie, String sexo, String descripcion, String foto,Cliente cliente) {
        this.nombre = nombre;
        this.edad = edad;
        this.raza = raza;
        this.especie = especie;
        this.sexo = sexo;
        this.descripcion = descripcion;
        this.foto = foto;
        this.cliente = cliente;
    }
    
    

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public int getIdMascota() {
        return idMascota;
    }

    public void setIdMascota(int idMascota) {
        this.idMascota = idMascota;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEdad() {
        return edad;
    }

    public void setEdad(String edad) {
        this.edad = edad;
    }

    public String getRaza() {
        return raza;
    }

    public void setRaza(String raza) {
        this.raza = raza;
    }

    public String getEspecie() {
        return especie;
    }

    public void setEspecie(String especie) {
        this.especie = especie;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
    public String getFoto(){
        return foto;
    }
    
    public void setFoto(String foto){
        this.foto = foto;
    }

    public int getEstatus() {
        return estatus;
    }

    public void setEstatus(int estatus) {
        this.estatus = estatus;
    }

    @Override
    public String toString() {
        return "Mascota{" + "idMascota=" + idMascota + ", nombre=" + nombre + ", edad=" + edad + ", raza=" + raza + ", especie=" + especie + ", sexo=" + sexo + ", descripcion=" + descripcion + ", estatus=" + estatus + ", cliente=" + cliente + '}';
    }
    
}
