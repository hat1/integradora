/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.utl.dsm501.happypet.model;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
/**
 *
 * @author Jonathan Ulises
 */
public class Cita {
    
    public static final DateTimeFormatter DTF
            = DateTimeFormat.forPattern("dd/MM/yyyy HH:mm:ss");

    private int idCita;
    private DateTime fechaIni;
    private DateTime fechafin;
    private String fechaHoraIniAsString;
    private String fechaHoraFinAsString;
    private int estatus;
    private Mascota mascota;
    private Cliente cliente;
    private Servicio servicio;

    public int getIdCita() {
        return idCita;
    }

    public void setIdCita(int idCita) {
        this.idCita = idCita;
    }

    public DateTime getFechaIni() {
        return fechaIni;
    }

    public void setFechaIni(DateTime fechaIni) {
        this.fechaIni = fechaIni;
    }

    public DateTime getFechafin() {
        return fechafin;
    }

    public void setFechafin(DateTime fechafin) {
        this.fechafin = fechafin;
    }

    public String getFechaHoraIniAsString() {
        return fechaHoraIniAsString;
    }

    public void setFechaHoraIniAsString(String fechaHoraIniAsString) {
        this.fechaHoraIniAsString = fechaHoraIniAsString;
    }

    public String getFechaHoraFinAsString() {
        return fechaHoraFinAsString;
    }

    public void setFechaHoraFinAsString(String fechaHoraFinAsString) {
        this.fechaHoraFinAsString = fechaHoraFinAsString;
    }

    public int getEstatus() {
        return estatus;
    }

    public void setEstatus(int estatus) {
        this.estatus = estatus;
    }

    public Mascota getMascota() {
        return mascota;
    }

    public void setMascota(Mascota mascota) {
        this.mascota = mascota;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Servicio getServicio() {
        return servicio;
    }

    public void setServicio(Servicio servicio) {
        this.servicio = servicio;
    }
    
    

    public Cita() {
    }

    public Cita(int idCita, DateTime fechaIni, DateTime fechafin, String fechaHoraIniAsString, String fechaHoraFinAsString, int estatus, Mascota mascota, Cliente cliente, Servicio servicio) {
        this.idCita = idCita;
        this.fechaIni = fechaIni;
        this.fechafin = fechafin;
        this.fechaHoraIniAsString = fechaHoraIniAsString;
        this.fechaHoraFinAsString = fechaHoraFinAsString;
        this.estatus = estatus;
        this.mascota = mascota;
        this.cliente = cliente;
        this.servicio = servicio;
    }

    public Cita(DateTime fechaIni, DateTime fechafin, String fechaHoraIniAsString, String fechaHoraFinAsString, int estatus, Mascota mascota, Cliente cliente, Servicio servicio) {
        this.fechaIni = fechaIni;
        this.fechafin = fechafin;
        this.fechaHoraIniAsString = fechaHoraIniAsString;
        this.fechaHoraFinAsString = fechaHoraFinAsString;
        this.estatus = estatus;
        this.mascota = mascota;
        this.cliente = cliente;
        this.servicio = servicio;
    }

    @Override
    public String toString() {
        return "Cita{" + "idCita=" + idCita + ", fechaIni=" + fechaIni + ", fechafin=" + fechafin + ", fechaHoraIniAsString=" + fechaHoraIniAsString + ", fechaHoraFinAsString=" + fechaHoraFinAsString + ", estatus=" + estatus + ", mascota=" + mascota + ", cliente=" + cliente + ", servicio=" + servicio + '}';
    }
    
    public Cita(int idCita, DateTime fechaIni, Mascota mascota, Servicio servicio) {
        this.idCita = idCita;
        this.fechaIni = fechaIni;
        this.mascota = mascota;
        this.servicio = servicio;
    }
}
