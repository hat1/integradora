/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.utl.dsm501.happypet.model;

/**
 *
 * @author PC
 */
public class Cliente {
    int idCliente;
    String correo; 		
    boolean estatus; 	
    Persona Persona;
    Usuario Usuario;	

    public Cliente() {
    }

    

    public Cliente(int idCliente, String correo, boolean estatus, Persona Persona, Usuario Usuario) {
        this.idCliente = idCliente;
        this.correo = correo;
        this.estatus = estatus;
        this.Persona = Persona;
        this.Usuario = Usuario;
    }

 

    public int getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public boolean isEstatus() {
        return estatus;
    }

    public void setEstatus(boolean estatus) {
        this.estatus = estatus;
    }

    public Persona getPersona() {
        return Persona;
    }

    public void setPersona(Persona Persona) {
        this.Persona = Persona;
    }

    public Usuario getUsuario() {
        return Usuario;
    }

    public void setUsuario(Usuario Usuario) {
        this.Usuario = Usuario;
    }

    @Override
    public String toString() {
        return "Cliente{" + "idCliente=" + idCliente + ", correo=" + correo + ", estatus=" + estatus + ", Persona=" + Persona + ", Usuario=" + Usuario + '}';
    }

    
    
}
