/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.utl.dsm501.happypet.rest;

import com.google.gson.Gson;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import edu.utl.dsm501.happypet.control.ControlServicio;
import edu.utl.dsm501.happypet.model.Servicio;
import java.util.List;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.core.Response;
/**
 *
 * @author Jonathan Ulises
 */
@Path("servicio")
public class RESTServicio {
    @Path("getAll")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAll(){
        ControlServicio cs = new ControlServicio();
        String out = "";
        try {
            List<Servicio> listSer = cs.getAllS();
            Gson gson = new Gson();
            out = gson.toJson(listSer);
        } catch (Exception e) {
            e.printStackTrace();
            out = "{\"error:\"" + e.toString() + "\"}";
        }
        
        return Response.status(Response.Status.OK).entity(out).build();
    }
    
    @Path("update")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response update(
            @FormParam("nomS") String nomSr,
            @FormParam("descS") String descSr,
            @FormParam("precioS") double preSr,
            @FormParam("tipoS") int typeSr,
            @FormParam("idS") int idSr){
        
        Servicio objS = new Servicio(idSr, nomSr, descSr, preSr, typeSr, 1);
        ControlServicio cs = new ControlServicio();
        String out = "";
        try {
            cs.updateServicio(objS);
            out = "{\"result\":\"OK\"}";
        } catch (Exception e) {
            out = "{\"error:\"" + e.toString() + "\"}";
        }
        
        return Response.status(Response.Status.OK).entity(out).build();
    }
    
    @Path("insert")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response insertServicio(
            @FormParam("nomS") String nomSr,
            @FormParam("descS") String descSr,
            @FormParam("precioS") double preSr,
            @FormParam("tipoS") int typeSr){
        
        ControlServicio cs = new ControlServicio();
        Servicio objS = new Servicio(nomSr, descSr, preSr, typeSr, 1);
            
        String out = "";
        try {
            cs.insertServicio(objS);
            out = "{\"result\":\"OK\"}";
        } catch (Exception e) {
            out = "{\"error:\"" + e.getMessage() + "\"}";
        }
        
        return Response.status(Response.Status.OK).entity(out).build();
    }
    
    
    @Path("delete")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteServicio(@FormParam("idS") int idS){
        ControlServicio cs = new ControlServicio();
        String out = "";
        try {
            cs.deleteServicio(idS);
            out = "{\"result\":\"OK\"}";
        } catch (Exception e) {
            out = "{\"error:\"" + e.getMessage() + "\"}";
        }
        
        return Response.status(Response.Status.OK).entity(out).build();
    }
    
    
    //Rest para servicios medicos
    @Path("getAllSM")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllSM(){
        ControlServicio cs = new ControlServicio();
        String out = "";
        try {
            List<Servicio> listSer = cs.getAllSM();
            Gson gson = new Gson();
            out = gson.toJson(listSer);
        } catch (Exception e) {
            e.printStackTrace();
            out = "{\"error:\"" + e.toString() + "\"}";
        }
        
        return Response.status(Response.Status.OK).entity(out).build();
    }
    
    //Rest para servicios Esteticos
    @Path("getAllSE")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllSE(){
        ControlServicio cs = new ControlServicio();
        String out = "";
        try {
            List<Servicio> listSer = cs.getAllSE();
            Gson gson = new Gson();
            out = gson.toJson(listSer);
        } catch (Exception e) {
            e.printStackTrace();
            out = "{\"error:\"" + e.toString() + "\"}";
        }
        
        return Response.status(Response.Status.OK).entity(out).build();
    }
    
}
