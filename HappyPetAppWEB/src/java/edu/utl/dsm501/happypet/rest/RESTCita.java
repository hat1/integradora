/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.utl.dsm501.happypet.rest;

import com.google.gson.Gson;
import edu.utl.dsm501.happypet.control.ControlCita;
import edu.utl.dsm501.happypet.model.Cita;
import edu.utl.dsm501.happypet.model.Cliente;
import edu.utl.dsm501.happypet.model.Mascota;
import edu.utl.dsm501.happypet.model.Persona;
import edu.utl.dsm501.happypet.model.Servicio;
import java.util.List;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
/**
 *
 * @author Jonathan Ulises
 */
@Path("cita")
public class RESTCita extends Application{
    @Path("getAll")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAll() {
        ControlCita cc = new ControlCita();
        String out = "";
        try {
            List<Cita> cita = cc.selectAll();
            Gson gson = new Gson();
            out = gson.toJson(cita);
        } catch (Exception e) {
            e.printStackTrace();
            out = "{\"error:\"" + e.toString() + "\"}";
        }
        return Response.status(Response.Status.OK).entity(out).build();
    }
    
    
    @Path("getAllCli")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllCli(@QueryParam("idCliente") int idCl) {
        ControlCita cc = new ControlCita();
        String out = "";
        try {
            List<Cita> cita = cc.selectOneCli(idCl);
            Gson gson = new Gson();
            out = gson.toJson(cita);
        } catch (Exception e) {
            e.printStackTrace();
            out = "{\"error:\"" + e.toString() + "\"}";
        }
        return Response.status(Response.Status.OK).entity(out).build();
    }
    

    @POST
    @Path("insert")
    @Produces(MediaType.APPLICATION_JSON)
    public Response insert(
            @FormParam("mascota") int mascota,
            @FormParam("cliente") int cliente,
            @FormParam("servicio") int servicio,
            @FormParam("fechaIni") String fechaIni
    ) {

        Mascota m = new Mascota();
        Cliente cl = new Cliente();
        Servicio s = new Servicio();

        m.setIdMascota(mascota);
        cl.setIdCliente(cliente);
        s.setIdServicio(servicio);

        Cita ci = new Cita();
        ci.setMascota(m);
        ci.setCliente(cl);
        ci.setServicio(s);
        ci.setFechaIni(ci.DTF.parseDateTime(fechaIni));

        ControlCita cc = new ControlCita();
        String out = "";
        try {
            cc.insert(ci);
            out = "{\"result\":\"OK\"}";
        } catch (Exception e) {
            e.printStackTrace();
            out = "{\"error:\"" + e.toString() + "\"}";  
        }
        return Response.status(Response.Status.OK).entity(out).build();
    }

    @Path("updateE")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateE(
            @FormParam("fechaIni") String fechaIni,
            @FormParam("mascota") int mascota,
            @FormParam("cliente") int cliente,
            @FormParam("servicio") int servicio,
            @FormParam("idCita") int idCi
    ) {
        Mascota m = new Mascota();
        Cliente cl = new Cliente();
        Servicio s = new Servicio();

        m.setIdMascota(mascota);
        cl.setIdCliente(cliente);
        s.setIdServicio(servicio);

        Cita ci = new Cita();
        ci.setFechaHoraIniAsString(fechaIni);
        ci.setMascota(m);
        ci.setCliente(cl);
        ci.setServicio(s);
        ci.setIdCita(idCi);

        ControlCita cc = new ControlCita();
        String out = "";

        try {
            cc.updateE(ci);
            out = "{\"result\":\"OK\"}";
        } catch (Exception er) {
            out = "{\"error:\"" + er.toString() + "\"}";
        }
        return Response.status(Response.Status.OK).entity(out).build();
    }

    @Path("updateCl")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateCl(
            @FormParam("fechaIni") String fechaIni,
            @FormParam("mascota") int mascota,
            @FormParam("servicio") int servicio,
            @FormParam("idCita") int idCi
    ) {
        Mascota m = new Mascota();
        Servicio s = new Servicio();

        m.setIdMascota(mascota);
        s.setIdServicio(servicio);

        Cita ci = new Cita();
        ci.setFechaIni(ci.DTF.parseDateTime(fechaIni));
        ci.setMascota(m);
        ci.setServicio(s);
        ci.setIdCita(idCi);

        ControlCita cc = new ControlCita();
        String out = "";

        try {
            cc.updateCl(ci);
            out = "{\"result\":\"OK\"}";
        } catch (Exception er) {
            out = "{\"error:\"" + er.toString() + "\"}";
        }
        return Response.status(Response.Status.OK).entity(out).build();
    }

    @Path("delete")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response delete(
            @FormParam("idCita") int idCita
    ) {
        Cita ci = new Cita();
        ci.setIdCita(idCita);
        
        ControlCita cc = new ControlCita();
        String out = "";
        try {
            cc.delete(idCita);
            out = "{\"result\":\"OK\"}";
        } catch (Exception e) {
            out = "{\"error:\"" + e.toString() + "\"}";
        }
        return Response.status(Response.Status.OK).entity(out).build();
    }
    
    @Path("changeEst")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response changeEst(
            @FormParam("idCita") int idCita,
            @FormParam("est") int est){
    
 
        ControlCita cc = new ControlCita();
        String out = "";
        try {
            cc.changeEst(idCita, est);
            out = "{\"result\":\"OK\"}";
        } catch (Exception e) {
            out = "{\"error:\"" + e.toString() + "\"}";
        }
        return Response.status(Response.Status.OK).entity(out).build();
    }
}
