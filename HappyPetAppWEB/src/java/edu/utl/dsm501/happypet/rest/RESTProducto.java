/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.utl.dsm501.happypet.rest;

import com.google.gson.Gson;
import edu.utl.dsm501.happypet.control.ControlProducto;
import edu.utl.dsm501.happypet.model.Producto;
import java.util.List;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author PC
 */
@Path("producto")
public class RESTProducto {

    @Path("getAll")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAll() {
        ControlProducto cp = new ControlProducto();
        String out = "";
        try {
            List<Producto> producto = cp.selectAll();
            Gson gson = new Gson();
            out = gson.toJson(producto);
        } catch (Exception e) {
            e.printStackTrace();
            out = "{\"error:\"" + e.toString() + "\"}";
        }
        return Response.status(Response.Status.OK).entity(out).build();
    }

    @POST
    @Path("insert")
    @Produces(MediaType.APPLICATION_JSON)
    public Response insert(
            @FormParam("nombrepr") String nomPr,
            @FormParam("descripccion") String des,
            @FormParam("precio") double pre,
            @FormParam("proveedor") String pro,
            @FormParam("foto") String fot
    ) {
        Producto objPr = new Producto();

        objPr.setNombrepr(nomPr);
        objPr.setDescripccion(des);
        objPr.setPrecio(pre);
        objPr.setProveedor(pro);
        objPr.setFoto(fot);

        ControlProducto cp = new ControlProducto();
        String out = "";
        try {
            cp.insertProducto(objPr);
            out = "{\"result\":\"OK\"}";
        } catch (Exception e) {
            e.printStackTrace();
            out = "{\"error:\"" + e.toString() + "\"}";
        }
        return Response.status(Response.Status.OK).entity(out).build();
    }

    @POST
    @Path("update")
    @Produces(MediaType.APPLICATION_JSON)
    public Response update(
            @FormParam("idPro") int idPr,
            @FormParam("nombrepr") String nomPr,
            @FormParam("descripccion") String des,
            @FormParam("precio") double pre,
            @FormParam("proveedor") String pro,
            @FormParam("fotoPr") String foto
    ) throws Exception {
        Producto objPr = new Producto();

        objPr.setIdProducto(idPr);
        objPr.setNombrepr(nomPr);
        objPr.setDescripccion(des);
        objPr.setPrecio(pre);
        objPr.setProveedor(pro);
        objPr.setFoto(foto);

        ControlProducto cp = new ControlProducto();
        String out = "";
        try {
            cp.updateProducto(objPr);
            out = "{\"result\":\"OK\"}";
        } catch (Exception er) {
            out = "{\"error:\"" + er.toString() + "\"}";
        }
        return Response.status(Response.Status.OK).entity((out)).build();
    }

    @Path("delete")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response delete(
            @FormParam("idPr") int idPr
    ) {
        Producto objPr = new Producto();
        objPr.setIdProducto(idPr);

        ControlProducto cp = new ControlProducto();
        String out = "";
        try {
            cp.deleteProducto(idPr);
            out = "{\"result\":\"OK\"}";
        } catch (Exception e) {
            out = "{\"error:\"" + e.toString() + "\"}";
        }
        return Response.status(Response.Status.OK).entity(out).build();
    }
}
