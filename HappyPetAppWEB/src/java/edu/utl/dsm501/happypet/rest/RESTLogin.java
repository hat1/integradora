/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.utl.dsm501.happypet.rest;

import edu.utl.dsm501.happypet.control.ControlLogin;
import edu.utl.dsm501.happypet.model.Cliente;
import java.util.List;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Jonathan Ulises
 */

@Path("login")
public class RESTLogin {
    
    @Path("verificar")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response searchUser(
            @FormParam("userN") String usu,
            @FormParam("passU") String pas
    ){
        ControlLogin cp = new ControlLogin();
        String out = "";
        int idC = 0;
        try {
            List<Cliente> clientesL = cp.searchUserLogin(usu, pas);
            if(!clientesL.isEmpty()){
                idC = clientesL.get(0).getIdCliente();
                out = "{\"result\":" + idC + "}";
            }else{
                out = "{\"result\":" + idC + "}";
            }
        } catch (Exception e) {
            e.printStackTrace();
            out = "{\"error:\"" + e.toString() + "\"}";
        }
        
        return Response.status(Response.Status.OK).entity(out).build();
    }
}
