
package edu.utl.dsm501.happypet.rest;

import java.util.List;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import com.google.gson.Gson;
import edu.utl.dsm501.happypet.control.ControlCliente;
import edu.utl.dsm501.happypet.model.Cliente;
import edu.utl.dsm501.happypet.model.Persona;
import edu.utl.dsm501.happypet.model.Usuario;
import javax.ws.rs.QueryParam;

/**
 *
 * @author PC
 */
@Path("cliente")
public class RESTCliente {

    @Path("getAll")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAll() {
        ControlCliente cc = new ControlCliente();
        String out = "";
        try {
            List<Cliente> cliente = cc.selectAll();
            Gson gson = new Gson();
            out = gson.toJson(cliente);
        } catch (Exception e) {
            e.printStackTrace();
            out = "{\"error:\"" + e.toString() + "\"}";
        }
        return Response.status(Response.Status.OK).entity(out).build();
    }

    @Path("getAllDelete")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllEliminados() {
        ControlCliente cc = new ControlCliente();
        String out = "";
        try {
            List<Cliente> cliente = cc.selectAllDelete();
            Gson gson = new Gson();
            out = gson.toJson(cliente);
        } catch (Exception e) {
            e.printStackTrace();
            out = "{\"error:\"" + e.toString() + "\"}";
        }
        return Response.status(Response.Status.OK).entity(out).build();
    }
    
    @Path("getOne")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getOne(@QueryParam("idC") int idC){
        ControlCliente cc = new ControlCliente();
        String out = "";
        try {
            Cliente cl = cc.selectOne(idC);
            Gson gson = new Gson();
            out = gson.toJson(cl);
        } catch (Exception e) {
            e.printStackTrace();
            out = "{\"error:\"" + e.toString() + "\"}";
        }
        return Response.status(Response.Status.OK).entity(out).build();
    }

    @POST
    @Path("insert")
    @Produces(MediaType.APPLICATION_JSON)
    public Response insert(
            @FormParam("mail") String email,
            @FormParam("nombre") String nom,
            @FormParam("apellidoP") String apeP,
            @FormParam("apellidoM") String apeM,
            @FormParam("genero") String gen,
            @FormParam("calle") String cal,
            @FormParam("colonia") String col,
            @FormParam("cp") int cp,
            @FormParam("telefono") String tel,
            @FormParam("nomUsuario") String nomUsuario,
            @FormParam("password") String pass
    ) {
        Cliente objC = new Cliente();
        Persona objP = new Persona();
        Usuario objU = new Usuario();
        objC.setCorreo(email);
        objP.setNombre(nom);
        objP.setApellidoPaterno(apeP);
        objP.setApellidoMaterno(apeM);
        objP.setGenero(gen);
        objP.setCalle(cal);
        objP.setColonia(col);
        objP.setCp(cp);
        objP.setTelefono(tel);
        objU.setNombreUsuario(nomUsuario);
        objU.setContrasena(pass);
        objC.setPersona(objP);
        objC.setUsuario(objU);

        ControlCliente cc = new ControlCliente();
        String out = "";
        int idC;
        try {
            idC = cc.insertCliente(objC);
            out = "{\"result\":" + idC + "}";
        } catch (Exception e) {
            e.printStackTrace();
            out = "{\"error:\"" + e.toString() + "\"}";
        }
        return Response.status(Response.Status.OK).entity(out).build();
    }

    @POST
    @Path("update")
    @Produces(MediaType.APPLICATION_JSON)
    public Response update(
            @FormParam("mail") String email,
            @FormParam("nombreA") String nom,
            @FormParam("apellidoP") String apeP,
            @FormParam("apellidoM") String apeM,
            @FormParam("genero") String gen,
            @FormParam("calle") String cal,
            @FormParam("colonia") String col,
            @FormParam("cp") int cp,
            @FormParam("telefono") String tel,
            @FormParam("nomUsuario") String nomUsuario,
            @FormParam("idC") int idC,
            @FormParam("idP") int idP,
            @FormParam("idU") int idU
    ) throws Exception {
        Cliente objC = new Cliente();
        Persona objP = new Persona();
        Usuario objU = new Usuario();
        objC.setCorreo(email);
        objP.setNombre(nom);
        objP.setApellidoPaterno(apeP);
        objP.setApellidoMaterno(apeM);
        objP.setGenero(gen);
        objP.setCalle(cal);
        objP.setColonia(col);
        objP.setCp(cp);
        objP.setTelefono(tel);
        objU.setNombreUsuario(nomUsuario);
        objP.setIdPersona(idP);
        objU.setIdUsuario(idU);
        objC.setPersona(objP);
        objC.setUsuario(objU);
        objC.setIdCliente(idC);

        ControlCliente cc = new ControlCliente();
        String out = "";
        try {
            cc.updateCliente(objC);
            out = "{\"result\":\"OK\"}";
        } catch (Exception er) {
            out = "{\"error:\"" + er.toString() + "\"}";
        }
        return Response.status(Response.Status.OK).entity((out)).build();
    }
    
   @Path("delete")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response delete(
            @FormParam("idC") int idC
    ) {
        Cliente objC = new Cliente();
        objC.setIdCliente(idC);

        ControlCliente cc = new ControlCliente();
        String out = "";
        try {
            cc.deleteCliente(idC);
            out = "{\"result\":\"OK\"}";
        } catch (Exception e) {
            out = "{\"error:\"" + e.toString() + "\"}";
        }        
        return Response.status(Response.Status.OK).entity(out).build();
    }
    
    @POST
    @Path("activate")
    @Produces(MediaType.APPLICATION_JSON)
    public Response activar(
            @FormParam("idC") int idC){
        ControlCliente cc = new ControlCliente();
        Cliente cliente = new Cliente();
        cliente.setIdCliente(idC);
        String out = "";
        try {
            cc.activateCliente(cliente);
            out = "{\"result\":\"OK\"}";
        } catch (Exception e) {
            e.printStackTrace();
            out = "{\"error:\"" + e.toString() + "\"}";
        }
        return Response.status(Response.Status.OK).entity(out).build();
    }
    
}
