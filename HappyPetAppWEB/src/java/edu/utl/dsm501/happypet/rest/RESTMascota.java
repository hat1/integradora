/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.utl.dsm501.happypet.rest;

import com.google.gson.Gson;
import edu.utl.dsm501.happypet.control.ControlMascota;
import edu.utl.dsm501.happypet.model.Cliente;
import edu.utl.dsm501.happypet.model.Mascota;
import java.util.List;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Jonathan Ulises
 */
@Path("mascota")
public class RESTMascota {
    @Path("getAllM")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllMascota(){
        String out = "";
        ControlMascota cm = new ControlMascota();
        
        try{
            List<Mascota> mas = cm.getAll();
            Gson gson = new Gson();
            out = gson.toJson(mas);
        } catch (Exception e){
            out = "{\"error:\"" + e.toString() + "\"}";
        }
        return Response.status(Response.Status.OK).entity(out).build();
    }
    
    @Path("getAllMCli")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllMCli(@QueryParam("idC") int idC){
        String out = "";
        ControlMascota cm = new ControlMascota();
        
        try{
            List<Mascota> mas = cm.getAllMascotaCl(idC);
            Gson gson = new Gson();
            out = gson.toJson(mas);
        } catch (Exception e){
            out = "{\"error:\"" + e.toString() + "\"}";
        }
        return Response.status(Response.Status.OK).entity(out).build();
    }
    
    @Path("getAllE")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllMascotaE(){
        String out = "";
        ControlMascota cm = new ControlMascota();
        
        try{
            List<Mascota> mas = cm.getAllE();
            Gson gson = new Gson();
            out = gson.toJson(mas);
        } catch (Exception e){
            out = "{\"error:\"" + e.toString() + "\"}";
        }
        return Response.status(Response.Status.OK).entity(out).build();
    }
     
    @Path("insert")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response insertMas(
            @FormParam("nombre") String nombre,
            @FormParam("edad") String edad,
            @FormParam("raza") String raza,
            @FormParam("especie") String especie,
            @FormParam("sexo") String sexo,
            @FormParam("descripcion") String descripcion,
            @FormParam("foto") String foto,
            @FormParam("cliente") int cliente
    ) {
        Mascota m = new Mascota();        
        Cliente c = new Cliente();
        c.setIdCliente(cliente);
        
        m.setNombre(nombre);
        m.setEdad(edad);
        m.setRaza(raza);
        m.setEspecie(especie);
        m.setSexo(sexo);
        m.setDescripcion(descripcion);
        m.setFoto(foto);
        m.setCliente(c);
        
        ControlMascota cm = new ControlMascota();
        String out = "";
        
        try {
            cm.insertMascota(m);
            out = "{\"result\":\"OK\"}";
        } catch (Exception e) {
          out = "{\"error:\"" + e.toString() + "\"}";  
        }
        return Response.status(Response.Status.OK).entity(out).build();
    }
    
    @Path("update")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response update(
            @FormParam("idMascota") int idMascota,
            @FormParam("nombre") String nombre,
            @FormParam("edad") String edad,
            @FormParam("raza") String raza,
            @FormParam("especie") String especie,
            @FormParam("sexo") String sexo,
            @FormParam("descripcion") String descripcion,
            @FormParam("foto") String foto,
            @FormParam("cliente") int cliente 
    ) {
        String out = "";
        System.out.println("" + nombre + edad + raza + especie + sexo + descripcion + + cliente);
        ControlMascota cm = new ControlMascota();
        Mascota m = new Mascota();
        
        Cliente c = new Cliente();
        c.setIdCliente(cliente);
        
        m.setIdMascota(idMascota);
        m.setNombre(nombre);
        m.setEdad(edad);
        m.setRaza(raza);
        m.setEspecie(especie);
        m.setSexo(sexo);
        m.setDescripcion(descripcion);
        m.setFoto(foto);
        m.setCliente(c);

        try {
            cm.update(m);
            out = "{\"result\":\"OK\"}";
        } catch(Exception e){
            out = "{\"error:\"" + e.toString() + "\"}";
        }
        return Response.status(Response.Status.OK).entity(out).build();
    }
    
    @Path("activar")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response activar(
            @FormParam("idMascota") int idMascota
    ) {
        String out = "";
        ControlMascota cm = new ControlMascota();
        
        Mascota m = new Mascota();
        m.setIdMascota(idMascota);
        
        try {
            cm.actMas(m);
            out = "{\"result\":\"OK\"}";
        } catch (Exception e) {
            out = "{\"error:\"" + e.toString() + "\"}";
        }
        return Response.status(Response.Status.OK).entity(out).build();
    }
    
    @Path("delete")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response delete(
        @FormParam("idMascota") int idMascota    
    ) {
        String out = "";
        ControlMascota cm = new ControlMascota();
        
        Mascota m = new Mascota();
        m.setIdMascota(idMascota);
        
        try {
            cm.delete(m);
            out = "{\"result\":\"OK\"}";
        } catch (Exception e) {
            out = "{\"error:\"" + e.toString() + "\"}";
        }
        return Response.status(Response.Status.OK).entity(out).build();
    }
}
