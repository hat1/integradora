/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.utl.dsm501.happypet.control;

import edu.utl.dsm501.happypet.model.Producto;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author PC
 */
public class ControlProducto {

    public ArrayList selectAll() {
        String query = "select * from vProducto where estatus = 1 order by nombrePr;";
        Conexion con = new Conexion();
        ArrayList<Producto> producto = new ArrayList<Producto>();
        try {
            con.abrir();
            Connection c = con.getConexion();
            Statement stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                Producto pr = new Producto();
                /*----  seccion de IDs  ----*/
                pr.setIdProducto(rs.getInt("idProducto"));

                /*----  Sección de Producto  ----*/
                pr.setNombrepr(rs.getString("nombrepr"));
                pr.setDescripccion(rs.getString("descripccion"));
                pr.setPrecio(rs.getInt("precio"));
                pr.setProveedor(rs.getString("proveedor"));
                pr.setFoto(rs.getString("foto"));
                pr.setEstatus(rs.getBoolean("estatus"));

                producto.add(pr);
            }
            rs.close();
            stmt.close();
            c.close();
            con.cerrar();
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        return producto;
    }

    public void insertProducto(Producto producto) {
        Conexion conexion = new Conexion();
        // metodo de insertar 
        try {
            Connection c = conexion.abrir();
            String sql = " call insertProducto(?,?,?,?,?);";
            try (CallableStatement stmt = (CallableStatement) c.prepareCall(sql)) {
                stmt.setString(1, producto.getNombrepr());
                stmt.setString(2, producto.getDescripccion());
                stmt.setDouble(3, producto.getPrecio());
                stmt.setString(4, producto.getProveedor());
                stmt.setString(5, producto.getFoto());
                stmt.executeUpdate();
            }
            conexion.cerrar();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateProducto(Producto producto) throws Exception {
        Conexion con = new Conexion();
        String query = "call updateProducto(?, ?, ?, ?, ?, ?);";
        try {
            con.abrir();
            Connection c = con.getConexion();
            CallableStatement stmt = c.prepareCall(query);
            stmt.setInt(1, producto.getIdProducto());
            stmt.setString(2, producto.getNombrepr());
            stmt.setString(3, producto.getDescripccion());
            stmt.setDouble(4, producto.getPrecio());
            stmt.setString(5, producto.getProveedor());
            stmt.setString(6, producto.getFoto());
            stmt.executeUpdate();
            stmt.close();
            c.close();
            con.cerrar();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteProducto(int idProducto) {
        Conexion c = new Conexion();
        String query = "call deleteProducto(?);";
        try {
            c.abrir();
            Connection con = c.getConexion();
            CallableStatement stmt = con.prepareCall(query);
            stmt.setInt(1, idProducto);
            stmt.execute();
            stmt.close();
            con.close();
            c.cerrar();
        } catch (Exception e) {
            e.toString();
        }
    }
}
