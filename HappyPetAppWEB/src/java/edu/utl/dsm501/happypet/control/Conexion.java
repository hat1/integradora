
package edu.utl.dsm501.happypet.control;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Conexion {

    String userName;
    String password;
    String url;
    Connection conexion;

    public Conexion() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            userName = "root";
            password = "root"; /*NO OLVIDEN PONER root SI ES QUE TU MySQL TIENE PASSWORD*/
            url = "jdbc:mysql://localhost:3306/happypet";
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public Connection abrir() throws Exception {
        conexion = DriverManager.getConnection(url, userName, password);
        return conexion;
    }

    public void cerrar() throws Exception {
        try {
            if (conexion != null) {
                conexion.close();
                conexion = null;
            }

        } catch (Exception e) {
        }
    }

    public Connection getConexion(){
        return conexion;
    }
}
