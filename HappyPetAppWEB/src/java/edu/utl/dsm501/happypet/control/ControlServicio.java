/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.utl.dsm501.happypet.control;

import edu.utl.dsm501.happypet.model.Servicio;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
/**
 *
 * @author Jonathan Ulises
 */
public class ControlServicio {
    
    public List getAllS() throws Exception{
        String sql = "SELECT * FROM vServicio WHERE estatus = 1;";
        Conexion con = new Conexion();
        List<Servicio> servicios = new ArrayList<>();
        Connection c = null;
        Statement stmt = null;
        try {
            con.abrir();
            c = con.getConexion();
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while(rs.next()){
                Servicio s = new Servicio();
                s.setIdServicio(rs.getInt(1));
                s.setNombreS(rs.getString(2));
                s.setDescrip(rs.getString(3));
                s.setPrecio(rs.getDouble(4));
                s.setTipos(rs.getInt(5));
                
                servicios.add(s);
            }
            
            rs.close();
            stmt.close();
            c.close();
            con.cerrar();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            
            con.cerrar();
        }
        
        return servicios;
    }
    
    public void insertServicio(Servicio s) throws Exception{
        String sql = "CALL insertServicio (?, ?, ?, ?);";
        Conexion objConn = new Conexion();
        Connection conn = null;
        CallableStatement stmt = null;
        
        
        try {
            conn = objConn.abrir();
            stmt = conn.prepareCall(sql);
            stmt.setString(1, s.getNombreS());
            stmt.setString(2, s.getDescrip());
            stmt.setDouble(3, s.getPrecio());
            stmt.setInt(4, s.getTipos());
            
            stmt.executeUpdate();
            
            stmt.close();
            conn.close();
            objConn.cerrar();
        } catch (SQLException e) {
            if(stmt != null){
                stmt.close();
            }
            
            conn.close();
            objConn.cerrar();
            e.printStackTrace();
           
        }
    }
    
    public void updateServicio(Servicio s) throws Exception {
        Conexion con = new Conexion();
        String sql = "CALL updateServicio(?, ?, ?, ?, ?);";
        try {
            con.abrir();
            Connection c = con.getConexion();
            CallableStatement stmt = c.prepareCall(sql);
            stmt.setString(1, s.getNombreS());
            stmt.setString(2, s.getDescrip());
            stmt.setDouble(3, s.getPrecio());
            stmt.setInt(4, s.getTipos());
            stmt.setInt(5, s.getIdServicio());
            
            stmt.executeUpdate();
            stmt.close();
            c.close();
            con.cerrar();
        } catch (SQLException e) {
            e.printStackTrace();
            con.cerrar();
        }
    }
    
    public void deleteServicio(int idS) throws Exception {
        String sql = "CALL deleteServicio (?);";
        Conexion objConn = new Conexion();
        Connection conn = null;
        CallableStatement stmt = null;
        
        try {
            conn = objConn.abrir();
            stmt = conn.prepareCall(sql);
            stmt.setInt(1, idS);
            stmt.executeUpdate();
            
            stmt.close();
            conn.close();
            objConn.cerrar();
        } catch (Exception e) {
            if(stmt != null){
                stmt.close();
            }
            
            conn.close();
            objConn.cerrar();
            e.printStackTrace();
        }
    }
    
    
    //GetAll para servicios medicos
    public List getAllSM() throws Exception{
        String sql = "SELECT * FROM vServicio WHERE estatus = 1 AND tipo = 1;";
        Conexion con = new Conexion();
        List<Servicio> servicios = new ArrayList<>();
        Connection c = null;
        Statement stmt = null;
        try {
            con.abrir();
            c = con.getConexion();
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while(rs.next()){
                Servicio s = new Servicio();
                s.setIdServicio(rs.getInt(1));
                s.setNombreS(rs.getString(2));
                s.setDescrip(rs.getString(3));
                s.setPrecio(rs.getDouble(4));
                s.setTipos(rs.getInt(5));
                
                servicios.add(s);
            }
            
            rs.close();
            stmt.close();
            c.close();
            con.cerrar();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            
            con.cerrar();
        }
        
        return servicios;
    }
    
    //getAll para servicios Esteticos
    public List getAllSE() throws Exception{
        String sql = "SELECT * FROM vServicio WHERE estatus = 1 AND tipo = 2;";
        Conexion con = new Conexion();
        List<Servicio> servicios = new ArrayList<>();
        Connection c = null;
        Statement stmt = null;
        try {
            con.abrir();
            c = con.getConexion();
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while(rs.next()){
                Servicio s = new Servicio();
                s.setIdServicio(rs.getInt(1));
                s.setNombreS(rs.getString(2));
                s.setDescrip(rs.getString(3));
                s.setPrecio(rs.getDouble(4));
                s.setTipos(rs.getInt(5));
                
                servicios.add(s);
            }
            
            rs.close();
            stmt.close();
            c.close();
            con.cerrar();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            
            con.cerrar();
        }
        
        return servicios;
    }
}
