/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.utl.dsm501.happypet.control;

import edu.utl.dsm501.happypet.model.Cliente;
import edu.utl.dsm501.happypet.model.Mascota;
import edu.utl.dsm501.happypet.model.Persona;
import edu.utl.dsm501.happypet.model.Usuario;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class ControlMascota {
    
    public ArrayList getAll() {
        String query = "SELECT * FROM vMascota WHERE estatus = 1 ORDER by idMascota;";
        Conexion c = new Conexion();

        ArrayList<Mascota> mascota = new ArrayList<>();
        try {
            c.abrir();
            Connection conn = c.getConexion();
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                Cliente cl = new Cliente();
                Mascota m = new Mascota();
                Persona p = new Persona();

                m.setIdMascota(rs.getInt("idMascota"));
                m.setNombre(rs.getString("nombreM"));
                m.setEdad(rs.getString("edad"));
                m.setRaza(rs.getString("raza"));
                m.setEspecie(rs.getString("especie"));
                m.setSexo(rs.getString("sexo"));
                m.setDescripcion(rs.getString("descripcion"));
                m.setFoto(rs.getString("foto"));
                m.setEstatus(rs.getInt("estatus"));
                cl.setIdCliente(rs.getInt("idCliente"));
                p.setIdPersona(rs.getInt("idPersona"));
                p.setNombre(rs.getString("nombre"));
                p.setApellidoPaterno(rs.getString("apellidoP"));
                p.setApellidoMaterno(rs.getString("apellidoM"));
                cl.setPersona(p);
                m.setCliente(cl);

                mascota.add(m);
            }
            rs.close();
            stmt.close();
            conn.close();
            c.cerrar();
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        return mascota;
    }

    public ArrayList getAllE()  {
        String query = "SELECT * FROM vMascota WHERE estatus = 0 ORDER by nombreM;";
        Conexion c = new Conexion();

        ArrayList<Mascota> mascota = new ArrayList<>();
        try {
            c.abrir();
            Connection conn = c.getConexion();
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                Cliente cl = new Cliente();
                Mascota m = new Mascota();
                Persona p = new Persona();

                m.setIdMascota(rs.getInt("idMascota"));
                m.setNombre(rs.getString("nombreM"));
                m.setEdad(rs.getString("edad"));
                m.setRaza(rs.getString("raza"));
                m.setEspecie(rs.getString("especie"));
                m.setSexo(rs.getString("sexo"));
                m.setDescripcion(rs.getString("descripcion"));
                m.setEstatus(rs.getInt("estatus"));
                cl.setIdCliente(rs.getInt("idCliente"));
                p.setIdPersona(rs.getInt("idPersona"));
                p.setNombre(rs.getString("nombre"));
                p.setApellidoPaterno(rs.getString("apellidoP"));
                p.setApellidoMaterno(rs.getString("apellidoM"));
                cl.setPersona(p);
                m.setCliente(cl);

                mascota.add(m);
            }
            rs.close();
            stmt.close();
            conn.close();
            c.cerrar();
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        return mascota;
    }
    
    
    public ArrayList getAllMascotaCl(int idCli)  {
        String query = "SELECT * FROM vMascota WHERE estatus = 1 AND idCliente = " + idCli + " ORDER by nombreM;";
        Conexion c = new Conexion();

        ArrayList<Mascota> mascota = new ArrayList<>();
        try {
            c.abrir();
            Connection conn = c.getConexion();
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                Cliente cl = new Cliente();
                Mascota m = new Mascota();
                Persona p = new Persona();

                m.setIdMascota(rs.getInt("idMascota"));
                m.setNombre(rs.getString("nombreM"));
                m.setEdad(rs.getString("edad"));
                m.setRaza(rs.getString("raza"));
                m.setEspecie(rs.getString("especie"));
                m.setSexo(rs.getString("sexo"));
                m.setDescripcion(rs.getString("descripcion"));
                m.setFoto(rs.getString("foto"));
                m.setEstatus(rs.getInt("estatus"));
                cl.setIdCliente(rs.getInt("idCliente"));
                p.setIdPersona(rs.getInt("idPersona"));
                p.setNombre(rs.getString("nombre"));
                p.setApellidoPaterno(rs.getString("apellidoP"));
                p.setApellidoMaterno(rs.getString("apellidoM"));
                cl.setPersona(p);
                m.setCliente(cl);

                mascota.add(m);
            }
            rs.close();
            stmt.close();
            conn.close();
            c.cerrar();
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        return mascota;
    }

    public void insertMascota(Mascota m) throws Exception {
        String query = "CALL insertMascota(?,?,?,?,?,?,?,?);";

        CallableStatement cstmt = null;
        Conexion c = new Conexion();
        Connection conn = null;

        try {
            conn = c.abrir();
            //conn.setAutoCommit(false);
            cstmt = conn.prepareCall(query);

            cstmt.setString(1, m.getNombre());
            cstmt.setString(2, m.getEdad());
            cstmt.setString(3, m.getRaza());
            cstmt.setString(4, m.getEspecie());
            cstmt.setString(5, m.getSexo());
            cstmt.setString(6, m.getDescripcion());
            cstmt.setString(7, m.getFoto());
            cstmt.setInt(8, m.getCliente().getIdCliente());

            cstmt.execute();
            //conn.commit();
            cstmt.close();
            c.cerrar();
        } catch (Exception ex) {
            if (cstmt != null) {
                cstmt.close();
            }
            c.cerrar();
            throw ex;
        }
    }

    public void update(Mascota m) throws Exception {
        String query = "CALL updateMascota(?,?,?,?,?,?,?,?,?);";

        CallableStatement cstmt = null;
        Conexion c = new Conexion();
        Connection conn = null;
        //System.out.println(e.toString()); 
        try {
            conn = c.abrir();
            conn.setAutoCommit(false);
            cstmt = conn.prepareCall(query);
            cstmt.setInt(1, m.getIdMascota());
            cstmt.setString(2, m.getNombre());
            cstmt.setString(3, m.getEdad());
            cstmt.setString(4, m.getRaza());
            cstmt.setString(5, m.getEspecie());
            cstmt.setString(6, m.getSexo());
            cstmt.setString(7, m.getDescripcion());
            cstmt.setString(8, m.getFoto());
            cstmt.setInt(9, m.getCliente().getIdCliente());

            cstmt.executeUpdate();
            conn.commit();
            cstmt.close();
            c.cerrar();
        } catch (Exception ex) {
            if (cstmt != null) {
                conn.rollback();
                cstmt.close();
            }

            c.cerrar();
            throw ex;
        }
    }

    public void delete(Mascota m) throws Exception {
        String sql = "CALL deleteMascota(?)";

        CallableStatement cstmt = null;
        Conexion c = new Conexion();
        Connection conn = null;

        try {
            conn = c.abrir();
            cstmt = conn.prepareCall(sql);
            cstmt.setInt(1, m.getIdMascota());
            cstmt.executeUpdate();

            cstmt.close();
            c.cerrar();
        } catch (Exception ex) {
            if (cstmt != null) {
                cstmt.close();
            }

            c.cerrar();
            throw ex;
        }
    }

    public void actMas(Mascota m) throws Exception {
        String sql = "CALL reactMascota(?)";

        CallableStatement cstmt = null;
        Conexion c = new Conexion();
        Connection conn = null;

        try {
            conn = c.abrir();
            cstmt = conn.prepareCall(sql);
            cstmt.setInt(1, m.getIdMascota());
            cstmt.executeUpdate();

            cstmt.close();
            c.cerrar();
        } catch (Exception ex) {
            if (cstmt != null) {
                cstmt.close();
            }

            c.cerrar();
            throw ex;
        }
    }
    
    public ArrayList<Cliente> buscarCli(String busqueda) {
        String query = "SELECT * FROM v_cliente WHERE nombre like '%" + busqueda + "%'"
                + "OR apellidoPaterno like '%" + busqueda + "%'"
                + "OR apellidoMaterno like '%" + busqueda + "%'";
        Conexion con = new Conexion();
        ArrayList<Cliente> cli = new ArrayList<Cliente>();

        try {
            con.abrir();
            Connection c = con.getConexion();
            Statement stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery(query);

            while (rs.next()) {
                Cliente cl = new Cliente();
                Persona p = new Persona();
                Usuario u = new Usuario();
                cl.setIdCliente(rs.getInt("idCliente"));
                p.setIdPersona(rs.getInt("idPersona"));
                u.setIdUsuario(rs.getInt("idUsuario"));
                p.setNombre(rs.getString("nombre"));
                p.setApellidoPaterno(rs.getString("apellidoP"));
                p.setApellidoMaterno(rs.getString("apellidoM"));
                p.setGenero(rs.getString("genero"));
                p.setTelefono(rs.getString("telefono"));
                p.setCalle(rs.getString("calle"));
                p.setColonia(rs.getString("colonia"));
                p.setCp(rs.getInt("cp"));
                u.setNombreUsuario(rs.getString("nombreUsuario"));
                cl.setCorreo(rs.getString("correo"));
                cl.setEstatus(rs.getBoolean("estatus"));
                u.setRol(rs.getString("rol"));
                cl.setPersona(p);
                cl.setUsuario(u);
                cli.add(cl);
            }
            rs.close();
            stmt.close();
            c.close();
            con.cerrar();
        } catch (Exception ex) {
            System.out.println(ex.toString());
        }
        return cli;
    }
    
    
}
