/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.utl.dsm501.happypet.control;

import edu.utl.dsm501.happypet.model.Cliente;
import edu.utl.dsm501.happypet.model.Persona;
import edu.utl.dsm501.happypet.model.Usuario;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;

/**
 *
 * @author PC
 */
public class ControlCliente {
    public ArrayList selectAll() {
        String query = "SELECT * FROM vCliente where estatus = 1 order by nombre;";
        Conexion con = new Conexion();
        ArrayList<Cliente> cliente = new ArrayList<Cliente>();
        try {
            con.abrir();
            Connection c = con.getConexion();
            Statement stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                Cliente cl = new Cliente();
                Persona p = new Persona();
                Usuario u = new Usuario();
                cl.setIdCliente(rs.getInt("idCliente"));
                p.setIdPersona(rs.getInt("idPersona"));
                u.setIdUsuario(rs.getInt("idUsuario"));
                p.setNombre(rs.getString("nombre"));
                p.setApellidoPaterno(rs.getString("apellidoP"));
                p.setApellidoMaterno(rs.getString("apellidoM"));
                p.setGenero(rs.getString("genero"));
                p.setTelefono(rs.getString("telefono"));
                p.setCalle(rs.getString("calle"));
                p.setColonia(rs.getString("colonia"));
                p.setCp(rs.getInt("cp"));
                u.setNombreUsuario(rs.getString("nombreUsuario"));
                cl.setCorreo(rs.getString("correo"));
                cl.setEstatus(rs.getBoolean("estatus"));
                u.setRol(rs.getString("rol"));
                cl.setPersona(p);
                cl.setUsuario(u);
                cliente.add(cl);
            }
            rs.close();
            stmt.close();
            c.close();
            con.cerrar();
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        return cliente;
    }
    
    public Cliente selectOne(int idC){
        String query = "SELECT * FROM vCliente where estatus = 1 and idCliente = " + idC + ";";
        Conexion con = new Conexion();
        Cliente objC = null;
        try {
            con.abrir();
            Connection c = con.getConexion();
            Statement stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                objC = new Cliente();
                Persona p = new Persona();
                Usuario u = new Usuario();
                objC.setIdCliente(rs.getInt("idCliente"));
                p.setIdPersona(rs.getInt("idPersona"));
                u.setIdUsuario(rs.getInt("idUsuario"));
                p.setNombre(rs.getString("nombre"));
                p.setApellidoPaterno(rs.getString("apellidoP"));
                p.setApellidoMaterno(rs.getString("apellidoM"));
                p.setGenero(rs.getString("genero"));
                p.setTelefono(rs.getString("telefono"));
                p.setCalle(rs.getString("calle"));
                p.setColonia(rs.getString("colonia"));
                p.setCp(rs.getInt("cp"));
                u.setNombreUsuario(rs.getString("nombreUsuario"));
                objC.setCorreo(rs.getString("correo"));
                objC.setEstatus(rs.getBoolean("estatus"));
                u.setRol(rs.getString("rol"));
                objC.setPersona(p);
                objC.setUsuario(u);
             
            }
            rs.close();
            stmt.close();
            c.close();
            con.cerrar();
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        return objC;
    }
    
    public ArrayList selectAllDelete() {
        String query = "SELECT * FROM vCliente where estatus = 0 order by nombre;";
        Conexion con = new Conexion();
        ArrayList<Cliente> cliente = new ArrayList<Cliente>();
        try {
            con.abrir();
            Connection c = con.getConexion();
            Statement stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                Cliente cl = new Cliente();
                Persona p = new Persona();
                Usuario u = new Usuario();
                cl.setIdCliente(rs.getInt("idCliente"));
                p.setIdPersona(rs.getInt("idPersona"));
                u.setIdUsuario(rs.getInt("idUsuario"));
                p.setNombre(rs.getString("nombre"));
                p.setApellidoPaterno(rs.getString("apellidoPaterno"));
                p.setApellidoMaterno(rs.getString("apellidoMaterno"));
                p.setGenero(rs.getString("genero"));
                p.setTelefono(rs.getString("telefono"));
                p.setCalle(rs.getString("calle"));
                p.setColonia(rs.getString("colonia"));
                p.setCp(rs.getInt("cp"));
                u.setNombreUsuario(rs.getString("nombre de usuario"));
                cl.setCorreo(rs.getString("correo"));
                cl.setEstatus(rs.getBoolean("estatus"));
                u.setRol(rs.getString("rol"));
                cl.setPersona(p);
                cl.setUsuario(u);
                cliente.add(cl);
            }
            rs.close();
            stmt.close();
            c.close();
            con.cerrar();
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        return cliente;
    }
    
    //Se modifico para que dovolviera el ID del cliente creado, esto para las
    //funcionalidades del registrar usuario de la aplicacion movil.
    public int insertCliente(Cliente cliente) {
        Conexion conexion = new Conexion();
        int idC = 0;
        // metodo de insertar 
        try {
            Connection c = conexion.abrir();
            String sql = " call insertCliente (?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
            try (CallableStatement stmt = (CallableStatement) c.prepareCall(sql)) {
                stmt.setString(1, cliente.getCorreo());
                stmt.setString(2, cliente.getPersona().getNombre());
                stmt.setString(3, cliente.getPersona().getApellidoPaterno());
                stmt.setString(4, cliente.getPersona().getApellidoMaterno());
                stmt.setString(5, cliente.getPersona().getGenero());
                stmt.setString(6, cliente.getPersona().getCalle());
                stmt.setString(7, cliente.getPersona().getColonia());
                stmt.setInt(8, cliente.getPersona().getCp());
                stmt.setString(9, cliente.getPersona().getTelefono());
                stmt.setString(10, cliente.getUsuario().getNombreUsuario());
                stmt.setString(11, cliente.getUsuario().getContrasena());
                stmt.registerOutParameter(12, Types.INTEGER);
                stmt.registerOutParameter(13, Types.INTEGER);
                stmt.registerOutParameter(14, Types.INTEGER);
                stmt.execute();
                
                idC = stmt.getInt(14);
            }
            conexion.cerrar();
        } catch (Exception e) {
            e.printStackTrace();
        }
         
        return idC;
    }
    
    
    public void updateCliente(Cliente cliente) throws Exception {
        Conexion con = new Conexion();
        String query = "call updateCliente(?,?,?,?,?,?,?,?,?,?,?,?,?);";
        try {
            con.abrir();
            Connection c = con.getConexion();
            CallableStatement stmt = c.prepareCall(query);
            stmt.setString(1, cliente.getCorreo());
            stmt.setString(2, cliente.getPersona().getNombre());
            stmt.setString(3, cliente.getPersona().getApellidoPaterno());
            stmt.setString(4, cliente.getPersona().getApellidoMaterno());
            stmt.setString(5, cliente.getPersona().getGenero());
            stmt.setString(6, cliente.getPersona().getCalle());
            stmt.setString(7, cliente.getPersona().getColonia());
            stmt.setInt(8, cliente.getPersona().getCp());
            stmt.setString(9, cliente.getPersona().getTelefono());
            stmt.setString(10, cliente.getUsuario().getNombreUsuario());
            stmt.setInt(11, cliente.getIdCliente());
            stmt.setInt(12, cliente.getPersona().getIdPersona());
            stmt.setInt(13, cliente.getUsuario().getIdUsuario());
            stmt.executeUpdate();
            stmt.close();
            c.close();
            con.cerrar();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    public void deleteCliente(int idCliente) {
        Conexion c = new Conexion();
        String query = "call deleteCliente (?);";
        try {
            c.abrir();
            Connection con = c.getConexion();
            CallableStatement stmt = con.prepareCall(query);
            stmt.setInt(1, idCliente);
            stmt.execute();
            stmt.close();
            con.close();
            c.cerrar();
        } catch (Exception e) {
            e.toString();
        }
    }
    
    public boolean activateCliente(Cliente cliente) throws Exception {
        boolean respuesta = false;
        String query = "call actiCliente (" + cliente.getIdCliente()+ ");";
        Conexion con = new Conexion();
        try {
            con.abrir();
            Connection c = con.getConexion();
            Statement stmt = c.createStatement();
            stmt.execute(query);
            stmt.close();
            c.close();
            con.cerrar();
        } catch (SQLException e) {
            e.printStackTrace();
            respuesta = false;
        }
        return respuesta;
    }
}
