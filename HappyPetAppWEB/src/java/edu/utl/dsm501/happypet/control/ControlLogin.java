/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.utl.dsm501.happypet.control;

import edu.utl.dsm501.happypet.model.Cliente;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Jonathan Ulises
 */
public class ControlLogin {
    
    public List searchUserLogin(String userN, String passU) throws Exception{
        String sql = "SELECT * FROM vClienteLog WHERE idUsuario IN "
                + "(SELECT idUsuario FROM vUsuLog WHERE nombreUsuario = '" 
                + userN + "' AND contrasena = '" + passU + "') AND estatus = 1;";
        Conexion con = new Conexion();
        List<Cliente> clientesLog = new ArrayList<>();
        Connection c = null;
        Statement stmt = null;
        
        try {
            con.abrir();
            c = con.getConexion();
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while(rs.next()){
                Cliente objC = new Cliente();
                objC.setIdCliente(rs.getInt(1));
                objC.setCorreo(rs.getString(2));
                
                clientesLog.add(objC);
            }
            
            rs.close();
            stmt.close();
            c.close();
            con.cerrar();
        } catch (Exception e) {
            if(stmt != null){
                stmt.close();
            }
            
            c.close();
            con.cerrar();
            e.printStackTrace();
        }
            
        return clientesLog;
    }
}
