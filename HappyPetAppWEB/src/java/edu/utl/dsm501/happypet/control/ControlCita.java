/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.utl.dsm501.happypet.control;

import edu.utl.dsm501.happypet.model.Cita;
import edu.utl.dsm501.happypet.model.Cliente;
import edu.utl.dsm501.happypet.model.Mascota;
import edu.utl.dsm501.happypet.model.Persona;
import edu.utl.dsm501.happypet.model.Servicio;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;

/**
 *
 * @author Jonathan Ulises
 */
public class ControlCita {
    
    public ArrayList selectAll() {
        String query = "select * from vCitas WHERE estatus_Cita = 1 ORDER BY nombre_Mascota;";
        Conexion con = new Conexion();
        ArrayList<Cita> cita = new ArrayList<Cita>();
        try {
            con.abrir();
            Connection c = con.getConexion();
            Statement stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                Cita ci = new Cita();
                Mascota m = new Mascota();
                Cliente cl = new Cliente();
                Servicio s = new Servicio();
                Persona pe = new Persona();

                ci.setIdCita(rs.getInt("idCita"));
                m.setIdMascota(rs.getInt("idMascota"));
                cl.setIdCliente(rs.getInt("idCliente"));
                pe.setIdPersona(rs.getInt("idPersona"));
                s.setIdServicio(rs.getInt("idServicio"));

                ci.setFechaHoraIniAsString(rs.getString("fechaIni"));
                ci.setFechaHoraFinAsString(rs.getString("fechaFin"));
                ci.setEstatus(rs.getInt("estatus_Cita"));

                m.setNombre(rs.getString("nombre_Mascota"));
                m.setEdad(rs.getString("edad"));
                m.setRaza(rs.getString("raza"));
                m.setEspecie(rs.getString("especie"));
                m.setSexo(rs.getString("sexo"));
                m.setDescripcion(rs.getString("descripcion"));
                m.setEstatus(rs.getInt("estatus_Mascota"));
                m.setFoto(rs.getString("foto"));

                pe.setNombre(rs.getString("nombre_Persona"));
                pe.setApellidoPaterno(rs.getString("apellidoP"));
                pe.setApellidoMaterno(rs.getString("apellidoM"));
                cl.setPersona(pe);

                s.setNombreS(rs.getString("nombreServ"));
                s.setDescrip(rs.getString("descripcionServ"));
                s.setPrecio(rs.getDouble("precio"));

                ci.setMascota(m);
                ci.setCliente(cl);
                ci.setServicio(s);

                cita.add(ci);
            }
            rs.close();
            stmt.close();
            c.close();
            con.cerrar();
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        return cita;
    }
    
    
    public ArrayList selectOneCli(int idCli) {
        String query = "select * from vCitas where idCliente = " + idCli + " ORDER BY nombre_Mascota;";
        Conexion con = new Conexion();
        ArrayList<Cita> cita = new ArrayList<Cita>();
        try {
            con.abrir();
            Connection c = con.getConexion();
            Statement stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                Cita ci = new Cita();
                Mascota m = new Mascota();
                Cliente cl = new Cliente();
                Servicio s = new Servicio();
                Persona pe = new Persona();

                ci.setIdCita(rs.getInt("idCita"));
                m.setIdMascota(rs.getInt("idMascota"));
                cl.setIdCliente(rs.getInt("idCliente"));
                pe.setIdPersona(rs.getInt("idPersona"));
                s.setIdServicio(rs.getInt("idServicio"));

                ci.setFechaHoraIniAsString(rs.getString("fechaIni"));
                ci.setFechaHoraFinAsString(rs.getString("fechaFin"));
                ci.setEstatus(rs.getInt("estatus_Cita"));
                

                m.setNombre(rs.getString("nombre_Mascota"));
                m.setEdad(rs.getString("edad"));
                m.setRaza(rs.getString("raza"));
                m.setEspecie(rs.getString("especie"));
                m.setSexo(rs.getString("sexo"));
                m.setFoto(rs.getString("foto"));

                pe.setNombre(rs.getString("nombre_Persona"));
                pe.setApellidoPaterno(rs.getString("apellidoP"));
                pe.setApellidoMaterno(rs.getString("apellidoM"));
                cl.setPersona(pe);

                s.setNombreS(rs.getString("nombreServ"));
                s.setDescrip(rs.getString("descripcionServ"));
                s.setPrecio(rs.getDouble("precio"));

                ci.setMascota(m);
                ci.setCliente(cl);
                ci.setServicio(s);

                cita.add(ci);
            }
            rs.close();
            stmt.close();
            c.close();
            con.cerrar();
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        return cita;
    }

    public void insert(Cita cita) throws Exception {
        Conexion con = new Conexion();
        String query = "call insertCita (?,?,?,?,?);";
        try {
            con.abrir();
            Connection c = con.getConexion();
            CallableStatement stmt = c.prepareCall(query);
            stmt.registerOutParameter(1, Types.INTEGER);
            stmt.setInt(2, cita.getMascota().getIdMascota());
            stmt.setInt(3, cita.getCliente().getIdCliente());
            stmt.setInt(4, cita.getServicio().getIdServicio());
            stmt.setString(5, cita.getFechaIni().toString("dd/MM/yyyy HH:mm:ss"));
            stmt.executeUpdate();
            cita.setIdCita(stmt.getInt(1));
            stmt.close();
            c.close();
            con.cerrar();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void updateE(Cita cita) throws Exception {
        Conexion con = new Conexion();
        String query = "call updateCitaEmpleado (?,?,?,?);";
        try {
            con.abrir();
            Connection c = con.getConexion();
            CallableStatement stmt = c.prepareCall(query);
            stmt.setString(1, cita.getFechaHoraIniAsString());
            stmt.setInt(2, cita.getMascota().getIdMascota());
            stmt.setInt(3, cita.getServicio().getIdServicio());
            stmt.setInt(4, cita.getIdCita());
            stmt.execute();
            stmt.close();
            c.close();
            con.cerrar();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void updateCl(Cita cita) throws Exception {
        Conexion con = new Conexion();
        String query = "call updateCitaCliente (?,?,?,?);";
        try {
            con.abrir();
            Connection c = con.getConexion();
            CallableStatement stmt = c.prepareCall(query);
            stmt.setString(1, cita.getFechaIni().toString("dd/MM/yyyy HH:mm:ss"));
            stmt.setInt(2, cita.getMascota().getIdMascota());
            stmt.setInt(3, cita.getServicio().getIdServicio());
            stmt.setInt(4, cita.getIdCita());
            stmt.execute();
            stmt.close();
            c.close();
            con.cerrar();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void delete(int idCita) {
        Conexion c = new Conexion();
        String query = "call deleteCita(?);";
        try {
            c.abrir();
            Connection con = c.getConexion();
            CallableStatement stmt = con.prepareCall(query);
            stmt.setInt(1, idCita);
            stmt.execute();
            stmt.close();
            con.close();
            c.cerrar();
        } catch (Exception e) {
            e.toString();
        }
    }
    
    public void changeEst(int idCita, int estatus) {
        Conexion c = new Conexion();
        String query = "call cambiarEstado(?, ?);";
        try {
            c.abrir();
            Connection con = c.getConexion();
            CallableStatement stmt = con.prepareCall(query);
            stmt.setInt(1, idCita);
            stmt.setInt(2, estatus);
            stmt.execute();
            stmt.close();
            con.close();
            c.cerrar();
        } catch (Exception e) {
            e.toString();
        }
    }
    
}
