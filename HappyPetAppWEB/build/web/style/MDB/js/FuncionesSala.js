var sala = null;
var salaActual = null;

function cargarModulosSalas() {
    $.ajax({
        type: 'GET',
        url: "salas/salas.html",
        async: true
    }).done(
            function (data) {
                $('$divMainContainer').html(data);
                actualizarTablaSalas();
            }
    );
}

function actualizarTablaSalas() {

    $.ajax(
            {
                type: "GET",
                async: true,
                url: "api/sala/getAll"
            }
    ).done(function (data) {
        sala = data;
        var str = '';
        
        for (var i = 0; i < sala.length; i++) {
            str += '<tr>' +
                    '<td>' + sala[i].nombre + '</td>' +
                    '<td>' + sala[i].descripcion + '</td>' +
                    '<td>' + sala[i].sucursales.nombre + '</td>' +
                    '<td><img width="70px" height="70px" src="data:;base64,' + sala[i].foto + '"></td>' +
                    '<td>' + sala[i].rutaFoto + '</td>' +
                   '<td><button onclick="mostrarDetalleSala(' + i + ');"\n\
                                class="btn btn-info btn-rounded" data-toggle="modal" data-target="#modificarSala"><i class="fas fa-info-circle"></i></button>'+'</td>' +
                                '</tr>';
            $('#tbSala').html(str);
        }
        selectSucursal(0,1);
        darAltaNuevaSala();
    });
}

function selectSucursal(idS,num) {
    var sucursales;
    var sucu= '';
    $.ajax(
            {
                type: "GET",
                async: true,
                url: "api/sucursal/getAll"
            }
    ).done(function(data)
            {
                    sucursales = data;
                    
                    for (var i = 0; i < sucursales.length; i++) {
                        
                        if (sucursales[i].idSucursal !== idS) {
                         sucu += '<option value="'+sucursales[i].idSucursal+'">'+sucursales[i].nombre+'</option>';
                     }else{
                           sucu += '<option value="'+sucursales[i].idSucursal+'"  selected="selected">'+sucursales[i].nombre+'</option>';
                     }

                    }
                    if (num == 1) {
                         $('#txtSucursalIdAgregar').html(sucu);
                    }else{
                         $('#txtSucursalId').html(sucu);
                    }

            });
            
}

function darAltaNuevaSala(){
     $("#txtSalaNombreAgregar").val('');
     $("#txtSalaDescripcionAgregar").val('');
     $("#txaBase64Agregar").val('');
     document.getElementById("imgFotoAgregar").value('');
     $("#txtFotoAgregar").val('');
     $("#txtSucursalIdAgregar").val('');

}

function mostrarDetalleSala(posicion) {
    if (posicion >= 0 && sala != null && sala[posicion] != null) {

        salaActual = sala[posicion];
        $('#txtSalaNombre').val(salaActual.nombre);
        $('#txtSalaDescripcion').val(salaActual.descripcion);
        document.getElementById("imgFoto").setAttribute("src", 'data:;base64,'+salaActual.foto);
        $('#txaBase64').val(salaActual.foto);
        $('#txtSalaId').val(salaActual.idSala);
         var sucu = salaActual.sucursales.idSucursal;
         selectSucursal(sucu,2);
        
    }
}

function guardarSala() {
    var nom = $("#txtSalaNombreAgregar").val();
    var des = $("#txtSalaDescripcionAgregar").val();
    var foto = $("#txaBase64Agregar").val();
    var ruta = $("#txtFotoAgregar").val();
    var idSucu = $("#txtSucursalIdAgregar").val();
    
    var data = {
        nom: nom,
        des: des,
        foto: foto,
        ruta: ruta,
        sucu: idSucu
    };
    $.ajax({
        type: "POST",
        url: "api/sala/insert",
        data: data,
        async: true
    }).done(function (data) {
       if (data.error != null) {
            
            Swal.fire(
                    'Ha ocurrido un Error',
                    data.error,
                    'error'
                    );
                return;
            }
             if (data.exception != null) {
            
            Swal.fire(
                    'Ha ocurrido un Excepcion',
                    data.exception,
                    'error'
                    );
                return;
            }
            Swal.fire({
                        type: 'success',
                        title: 'Movimiento realizado',
                        text:  'La sala se guardo con éxito',
                        showConfirmButton: false,
                        timer : 1500
                    });
        actualizarTablaSalas();
    });
}

function modificarSala() {
    var id = $("#txtSalaId").val();
    var nom = $("#txtSalaNombre").val();
    var des = $("#txtSalaDescripcion").val();
    var foto = $("#txaBase64").val();
    var ruta = $("#txtFoto").val();
    var sucu = $("#txtSucursalId").val();

    var data = {
        id: id,
        des: des,
        nom: nom,
        foto: foto,
        ruta: ruta,
        sucu:sucu
    };
    $.ajax({
        type: "POST",
        url: "api/sala/update",
        data: data,
        async: true
    }).done(function (data) {
       if (data.error != null) {
            
            Swal.fire(
                    'Ha ocurrido un Error',
                    data.error,
                    'error'
                    );
                return;
            }
             if (data.exception != null) {
            
            Swal.fire(
                    'Ha ocurrido un Excepcion',
                    data.exception,
                    'error'
                    );
                return;
            }
            Swal.fire({
                        type: 'success',
                        title: 'Movimiento realizado',
                        text:  'La sala se modificó con éxito',
                        showConfirmButton: false,
                        timer : 1500
                    });
        actualizarTablaSalas();
    });
}

function eliminarSala() {
    var id = salaActual.idSala;
    var data = {id: id};

    $.ajax({
        type: "POST",
        url: "api/sala/delete",
        data: data,
        async: true
    }).done(function (data) {
        if (data.error != null) {
            
            Swal.fire(
                    'Ha ocurrido un Error',
                    data.error,
                    'error'
                    );
                return;
            }
             if (data.exception != null) {
            
            Swal.fire(
                    'Ha ocurrido un Excepcion',
                    data.exception,
                    'error'
                    );
                return;
            }
            Swal.fire({
                        type: 'success',
                        title: 'Movimiento realizado',
                        text:  'La sala se elimino con éxito',
                        showConfirmButton: false,
                        timer : 1500
                    });
        actualizarTablaSalas();
    });
}

function buscarTablaSalas(){
    
    var word = $("#txtBuscar").val();
    $('#txtReturn').show();

    $.ajax(
            {
                type: "POST",
                url: "api/sala/search",
                async: true,
                data:{wordSearch: word}
            }).done(function (data) {
                
        sala = data;
        var str = '';
        
        if (sala.length > 0 ) {
        for (var i = 0; i < sala.length; i++) {
            str += '<tr>' +
                    '<td>' + sala[i].nombre + '</td>' +
                    '<td>' + sala[i].descripcion + '</td>' +
                    '<td>' + sala[i].sucursales.nombre + '</td>' +
                    '<td><img width="70px" height="70px" src="data:;base64,' + sala[i].foto + '"></td>' +
                    '<td>' + sala[i].rutaFoto + '</td>' +
                   '<td><button onclick="mostrarDetalleSala(' + i + ');"\n\
                        class="btn btn-info btn-rounded" data-toggle="modal" data-target="#modificarSala"><i class="fas fa-info-circle"></i></button>'+'</td>' +
                    '</tr>';
            $('#tbSala').html(str);
        }
        }else{
            str =  '<tr>' +
                                '<td>' + "" + '</td>' +
                                '<td>' + "" + '</td>' +
                                '<td>' + "No se encontraron registros coincidentes" + '</td>' +
                                '<td>' + "" + '</td>' +
                                '<td>' + "" +'</td>' +
                                '<td>' + "" + '</td>' +
                                '</tr>';
                    $('#tbSala').html(str);
                    
                     Swal.fire({
                        type: 'error',
                        title: 'Sin resultados',
                        text:  'No se encontraron resultados con tu busqueda',
                        showConfirmButton: false,
                        timer : 2000
                    });
        }
        selectSucursal(0,1);
        darAltaNuevaSala();
    });
}
function btnReturn(){
    actualizarTablaSalas();
    $('#txtReturn').hide();
    $('#txtBuscar').val('');
}
function cargarFoto(){
    var fileChooser=document.getElementById("txtFoto");
    var foto=document.getElementById("imgFoto");
    var base64=document.getElementById("txaBase64");
    if (fileChooser.files.length > 0) {
        
        var fr = new FileReader();
        
        fr.onload = function (){
            foto.src = "";
            foto.src=fr.result;
            base64.value = "";
            base64.value=foto.src.replace(/^data:image\/(png|jpg|jpeg);base64,/,'');
            
        };
        fr.readAsDataURL(fileChooser.files[0]);
    }
}

function cargarFotoAgregar(){
    var fileChooser=document.getElementById("txtFotoAgregar");
    var foto=document.getElementById("imgFotoAgregar");
    var base64=document.getElementById("txaBase64Agregar");
    if (fileChooser.files.length > 0) {
        
        var fr = new FileReader();
        
        fr.onload = function (){
            foto.src = "";
            foto.src=fr.result;
            base64.value = "";
            base64.value=foto.src.replace(/^data:image\/(png|jpg|jpeg);base64,/,'');
            
        };
        fr.readAsDataURL(fileChooser.files[0]);
    }
}

