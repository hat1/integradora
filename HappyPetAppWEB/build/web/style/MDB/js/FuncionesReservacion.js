var reservacion = null;
var reservacionActual = null;

function fechaHoy(){
    var hoy = new Date();
        var dd = hoy.getDate();
        var mm = hoy.getMonth()+1;
        var yyyy = hoy.getFullYear();
        
        dd = addZero(dd);
        mm = addZero(mm);
    var diaHoy = yyyy+'-'+mm+'-'+dd;
        return diaHoy;
}
function addZero(i) {
    if (i < 10) {
        i = '0' + i;
    }
    return i;
}

function actualizarTablaReservacionClientes() {
    
    var word = $('#txtBuscarCliente').val();
    $.ajax(
            {
                type: "POST",
                async: true,
                url: "api/reservacion/searchCliente",
                data: {
                    wordSearch : word
                }
            }
    ).done(function(data)
            {
                    reservacion = data;
                    var str = '';
                if(reservacion.length > 0 ){
                    for (var i = 0; i < reservacion.length; i++) {
                        str += '<tr>' +
                                '<td>' + reservacion[i].persona.nombre+' '+reservacion[i].persona.apellidoP+' '+reservacion[i].persona.apellidoM+ '</td>' +
                                '<td><button  "\n\
                                class="btn btn-info btn-rounded" data-toggle="modal" data-target="#modificarCliente" onclick="mostrarNombreCliente('+ i +')"><i class="fas fa-plus-square"></i></button>'+'</td>' +
                                '</tr>';
                        
                        $('#tbReservacion').html(str);
                        selectSucursal(0);
                    }
                    
                  }else{

                    str =  '<tr>' +
                                
                                '<td>' + "" + '</td>' +
                                '<td>' + "No se encontraron coincidencias" + '</td>' +
                                '</tr>';
                    $('#tbReservacion').html(str);
                    //txtReservacionCliente
                     Swal.fire({
                        type: 'error',
                        title: 'Sin resultados',
                        text:  'No se encontraron resultados con tu busqueda',
                        showConfirmButton: false,
                        timer : 2000
                    });
                }
            });
            document.getElementById("txtReservacionDate").setAttribute("min", fechaHoy());
}

function selectSucursal(idS) {
    
    var sucursales;
    var sucu= '';
    $.ajax(
            {
                type: "GET",
                async: true,
                url: "api/reservacion/getAllSucursal"
            }
    ).done(function(data)
            {
                    sucursales = data;
                    var idSuc;
                    for (var i = 0; i < sucursales.length; i++) {
                        idSuc = sucursales[i].idSucursal;
                     if (sucursales[i].idSucursal !== idS) {
                         sucu += '<option value="'+ idSuc+'">'+sucursales[i].nombre+'</option>';
                     }else{
                           sucu += '<option value="'+ idSuc+'"  selected="selected">'+sucursales[i].nombre+'</option>';
                     }

                    }
                         $('#txtReservacionSucursal').html(sucu);
                    selectSala();
            });
             
}

function selectSala() {
    var idS = $('#txtReservacionSucursal').val();
    
    var sala = null;
    var sucu= '';
    
    $.ajax(
            {
                type: "POST",
                async: true,
                url: "api/reservacion/getAllSalas",
                data:{
                    num : idS
                }

            }
    ).done(function(data)
            {
                    sala = data;
                    for (var i = 0; i < sala.length; i++) {
                        
                         sucu += '<option value="'+sala[i].idSala+'">'+sala[i].nombre+'</option>';
                    }
                         $('#txtReservacionSala').html(sucu);
            });  
}

function selectHorario() {
    
    var idS = $('#txtReservacionSala').val();
    var fecha = $('#txtReservacionDate').val();
    var horario = null;
    var sucu= '';
    
    $.ajax(
            {
                type: "POST",
                async: true,
                url: "api/reservacion/getAllHorario",
                data:{
                    idS : idS,
                    fecha : fecha
                }

            }
    ).done(function(data)
            {
                    horario = data;
                    for (var i = 0; i < horario.length; i++) {
                        
                         sucu += '<option value="'+horario[i].horaInicio+'-'+horario[i].horaFin+'">'+horario[i].horaInicio+'-'+horario[i].horaFin+'</option>';
                    }
                         $('#txtReservacionHorario').html(sucu);
            }); 
}

function  guardarReservacion(){
    
    var idCliente = $('#txtReservacionIdCliente').val();
    var idSala = $('#txtReservacionSala').val();
    var fecha = $('#txtReservacionDate').val();
    var idHorario = $('#txtReservacionHorario').val();
    
    var fechHoraInicio = fecha +' '+ idHorario.substring(0,8);
    var fechHoraFin = fecha +' '+ idHorario.substring(9,idHorario.length);
    
    var data = {
                
                idCli : idCliente,
                idSala : idSala,
                fechaHI : fechHoraInicio,
                fechaHF : fechHoraFin
        };
        
    $.ajax({
            type : "POST",
            url : "api/reservacion/insert",
            data : data,
            async: true
        }).done(function (data){
            if (data.error != null) {
            
            Swal.fire(
                    'Ha ocurrido un Error',
                    data.error,
                    'error'
                    );
                return;
            }
             if (data.exception != null) {
            
            Swal.fire(
                    'Ha ocurrido un Excepcion',
                    data.exception,
                    'error'
                    );
                return;
            }
            Swal.fire({
                        type: 'success',
                        title: 'Movimiento realizado',
                        text:  'Cliente se guardo con éxito',
                        showConfirmButton: false,
                        timer : 1500
                    });
        
           // actualizarTablaClientes();
           // darAltaNuevoCliente();
        });
    $('#txtBuscarCliente').val('');
    $('#tbReservacion').html('');
    $('#txtReservacionIdCliente').html('');
    $('#txtReservacionSala').html('');
    $('#txtReservacionDate').val('');
    $('#txtReservacionHorario').html('');
    cerrarModal();
    
  
}

function validarDatosReservacion(){
    if($('#txtIdClienteReservacion').val().length){
        
    }
}

function cerrarModal(){
    $(function () {
      $('#modalBuscarCliente').modal('toggle');
     });
}

function  mostrarNombreCliente(posicion){
    if (posicion >= 0 && reservacion!== null && reservacion[posicion] !== null) {
        
        clienteActual = reservacion[posicion];
        $('#txtReservacionCliente').val(clienteActual.persona.nombre+' '+clienteActual.persona.apellidoP+' '+clienteActual.persona.apellidoM);
        $('#txtReservacionIdCliente').val(clienteActual.idCliente);
    }
}
