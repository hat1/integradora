var sucursales = null;
var sucursalActual = null;
var mapa = null;

function inicializarMapa(lat,lng,nom){
    //var google = "http://maps.googleapis.com/maps/api/js?key=AIzaSyAj0PiGOP5OUYwxZny7QWOOBN2AI8hspL4";
     var latlng = new google.maps.LatLng(lat, lng);
                var opciones = {
                    zoom: 16,
                    center: latlng,
                    mapTypeId: google.maps.MapTypeId.ROADMAP};
                
               var map = new google.maps.Map(document.getElementById("map"), opciones);
               //Aqui empieza practica 2
               var infoWindow1 = new google.maps.InfoWindow;
               var onMarketMouseOver = function(){
                   var latLng = this.getPosition();
                   infoWindow1.setContent('<h3>'+nom+'</h3>  Latitud='+latLng.lat()+', Longitud='+latLng.lng());
                   infoWindow1.open(map,this);
               }; 
               google.maps.event.addListener(map,'mouseout',function (){
                   infoWindow1.close();
                  // 21.0934255,-101.6743251
               });
               var maker1 = new google.maps.Marker({
                   map: map,
                   position: new google.maps.LatLng(lat, lng)
               });
               
               google.maps.event.addListener(maker1,'mouseover', onMarketMouseOver);
               //----------------------------------------------------------------------------------------------
           
 }
 
 function cargarModulosSucursales() {

    $.ajax({
        type: 'GET',
        url: "/catalago.html",
        async: true
    }).done(
            function (data) {
                $('$divMainContainer').html(data);
                actualizarTablaSucursales();
            }
    );
}
 
function actualizarTablaSucursales() {
    
    $.ajax(
            {
                type: "GET",
                async: true,
                url: "api/sucursal/getAll"
            }
    ).done(function(data)
            {
                 

                    sucursales = data;
                    var str = '';
                    for (var i = 0; i < sucursales.length; i++) {
                        str += '<tr>' +
                                '<td>' + sucursales[i].nombre + '</td>' +
                                '<td>' + sucursales[i].direccion + '</td>' +
                                '<td>' + sucursales[i].latitud + '</td>' +
                                '<td>' + sucursales[i].longitud + '</td>' +
                                '<td><button onclick="mostrarDetalleSucursal(' + i + ');"\n\
                                class="btn btn-info btn-rounded"><i class="fas fa-info-circle"></i></button>'+'</td>' +
                                '</tr>';
                        
                        $('#tbSucursal').html(str);
                        

                    }
            });
}

function filtrarTablaSucursales() {
    
    var word = $('#txtBuscar').val();
    $('#txtReturn').show();
    $.ajax(
            
            {

                type: "POST",
                async: true,
                url: "api/sucursal/search",
                data:{
                    wordSearch : word
                }
                
            }
    ).done(function(data)
            {
                 sucursales = data;
                 
                    var str = '';
                 if (sucursales.length > 0 ) {
                    for (var i = 0; i < sucursales.length; i++) {
                        str += '<tr>' +
                                '<td>' + sucursales[i].nombre + '</td>' +
                                '<td>' + sucursales[i].direccion + '</td>' +
                                '<td>' + sucursales[i].latitud + '</td>' +
                                '<td>' + sucursales[i].longitud + '</td>' +
                                '<td><button onclick="mostrarDetalleSucursal(' + i + ');"\n\
                                class="btn btn-info btn-rounded"><i class="fas fa-info-circle"></i></button>'+'</td>' +
                                '</tr>';
                        
                        $('#tbSucursal').html(str);
                    }
                }else{

                    str =  '<tr>' +
                                '<td>' + "" + '</td>' +
                                '<td>' + "" + '</td>' +
                                '<td>' + "No se encontraron registros coincidentes" + '</td>' +
                                '<td>' + "" + '</td>' +
                                '<td>' + "" +'</td>' +
                                '</tr>';
                    $('#tbSucursal').html(str);
                    
                    Swal.fire({
                        type: 'error',
                        title: 'Sin resultados',
                        text:  'No se encontraron resultados con tu busqueda',
                        showConfirmButton: false,
                        timer : 2000
                    });
                }
                    
            });
            
   
}

function btnReturn(){
    actualizarTablaSucursales();
    $('#txtReturn').hide();
    $('#txtBuscar').val('');
}

function  mostrarDetalleSucursal(posicion){
    if (posicion >= 0 && sucursales !== null && sucursales[posicion] !== null) {
        
        sucursalActual = sucursales[posicion];
        $('#txtSucursalNombre').val(sucursalActual.nombre);
        $('#txtSucursalDomicilio').val(sucursalActual.direccion);
        $('#txtSucursalLatitud').val(sucursalActual.latitud);
        $('#txtSucursalLongitud').val(sucursalActual.longitud);
        $('#txtSucursalId').val(sucursalActual.idSucursal);
        
        $('#divSucursalCatalago').removeClass("col-sm-12");
        $('#divSucursalCatalago').addClass("col-sm-6");
        $('#divSucursalDetalle').show();
        
        $("#geo").show();
        
        if (mapa === null) {
            //se inicializa el mapa
            inicializarMapa(sucursalActual.latitud,sucursalActual.longitud,sucursalActual.nombre);
        }else{
            sucursalActual = null;
            
            $('#divSucursalDetalle').hide();
            $('divSucursalCatalago').removeClass("col-sm-12");
            $('divSucursalCatalago').addClass('col-sm-6');
        }
    }
}

function cerrarDetalleSucursal(){
    sucursalActual = null;
    $('#divSucursalCatalago').removeClass("col-sm-6");
    $('#divSucursalCatalago').addClass("col-sm-12");
    $('#divSucursalDetalle').hide();
    
}
    
    function cerrarDetalleSucursalAgregar(){

    $('#divSucursalCatalago').removeClass("col-sm-6");
    $('#divSucursalCatalago').addClass("col-sm-12");
    $('#divSucursalDetalleAgregar').hide();
    
}

function guardarSucursal(){
    
        var nom = $('#txtSucursalNombreAgregar').val();
        var dom = $('#txtSucursalDomicilioAgregar').val();
        var lat = $('#txtSucursalLatitudAgregar').val();
        var lng = $('#txtSucursalLongitudAgregar').val();
        
        var data = {
                
                nom : nom,
                dom : dom,
                lat : lat,
                lng : lng
        };
        
        $.ajax({
            type : "POST",
            url : "api/sucursal/insert",
            data : data,
            async: true
        }).done(function (data){
            if (data.error != null) {
            
            Swal.fire(
                    'Ha ocurrido un Error',
                    data.error,
                    'error'
                    );
                return;
            }
             if (data.exception != null) {
            
            Swal.fire(
                    'Ha ocurrido un Excepcion',
                    data.exception,
                    'error'
                    );
                return;
            }
            Swal.fire({
                type: 'success',
                title: 'Movimiento realizado',
                text:  'Sucursal agregada con éxito',
                showConfirmButton: false,
                timer : 1500
            });
        
            actualizarTablaSucursales();
            darAltaNuevaSucursal();
        });
}

function modificarSucursal(){
        
        var id = $('#txtSucursalId').val();
        var nom = $('#txtSucursalNombre').val();
        var dom = $('#txtSucursalDomicilio').val();
        var lat = $('#txtSucursalLatitud').val();
        var lng = $('#txtSucursalLongitud').val();
        
        var data = {
                
                id : id,
                nom : nom,
                dom : dom,
                lat : lat,
                lng : lng
        };
        
        $.ajax({
            type : "POST",
            url : "api/sucursal/update",
            data : data,
            async: true
        }).done(function (data){
            if (data.error != null) {
            
            Swal.fire(
                    'Ha ocurrido un Error',
                    data.error,
                    'error'
                    );
                return;
            }
             if (data.exception != null) {
            
            Swal.fire(
                    'Ha ocurrido un Excepcion',
                    data.exception,
                    'error'
                    );
                return;
            }
            Swal.fire({
                type: 'success',
                title: 'Movimiento realizado',
                text:  'Sucursal modificada con éxito',
                showConfirmButton: false,
                timer : 1500
            });
        
            actualizarTablaSucursales();
            cerrarDetalleSucursal();
        });
}

function darAltaNuevaSucursal(){
     $('#txtSucursalNombreAgregar').val('');
     $('#txtSucursalDomicilioAgregar').val('');
     $('#txtSucursalLatitudAgregar').val('');
     $('#txtSucursalLongitudAgregar').val('');
}
    
function eliminarSucursal(){
        
        var id = $('#txtSucursalId').val();
        
        var data = {
                id : id
        };
        
        $.ajax({
            type : "POST",
            url : "api/sucursal/delete",
            data : data,
            async: true
        }).done(function (data){
            if (data.error != null) {
            
            Swal.fire(
                    'Ha ocurrido un Error',
                    data.error,
                    'error'
                    );
                return;
            }
             if (data.exception != null) {
            
            Swal.fire(
                    'Ha ocurrido un Excepcion',
                    data.exception,
                    'error'
                    );
                return;
            }
            Swal.fire({
                type: 'success',
                title: 'Movimiento realizado',
                text:  'Sucursal eliminada con éxito',
                showConfirmButton: false,
                timer : 1500
            });
        
            actualizarTablaSucursales();
            cerrarDetalleSucursal();
    });
}