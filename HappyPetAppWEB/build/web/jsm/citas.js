/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var servicios = null;
var cita = null;
var horarios = null;
var mascotaC = null;
var reservacions = null;
var cliente=null;
var clienteActualList = null;

var servicioActual = null;

function cargarModuloReservacion(){
    $.ajax(
            {
                type: "GET",
                url: "html/citas.html",
                async: true
            }
    ).done(function (data) {
        $('#principal').html(data);
        actualizarTablaServicios();
    });
}

function actualizarTablaServicios() {
    $.ajax(
            {
                type: "GET",
                async: true,
                url: "api/cita/getAll"
            }
    ).done(function (data)
    {
        cita = data;
        var str = '';
    for (var i = 0; i < cita.length; i++) {
        h = hora(cita[i].fechaHoraIniAsString);
        f = fecha(cita[i].fechaHoraIniAsString);       
        hf = horaF(cita[i].fechaHoraFinAsString);
            str += '<tr>' +
                    '<td>' + cita[i].cliente.Persona.nombre + " "+ cita[i].cliente.Persona.apellidoPaterno+" "+ cita[i].cliente.Persona.apellidoMaterno+'</td>' +
                    '<td>'+cita[i].mascota.nombre + '</td>' +
                    '<td>'+f + '</td>' +
                    '<td>'+h + '</td>' +
                    '<td>'+hf + '</td>' +
                    '<td>'+ cita[i].servicio.nombreS + '</td>' +
                    '<td>'+"$" + cita[i].servicio.precio + '</td>' +
                '<td><button  "\n\
                class="btn btn-info btn-rounded"  onclick="mostrarDetalleCita('+ i +')">\n\
               <i class="fas fa-plus-square"></i></button>'+'</td>' +
                
                '<td><button  "\n\
                class="btn btn-warning btn-rounded"  onclick="cancelarCita('+ cita[i].idCita +')">\n\
               Cancelar Cita</button>'+'</td>' +
                '<td><button  "\n\
                class="btn btn-success btn-rounded"  onclick="terminarCita('+ cita[i].idCita +')">\n\
               Terminar Cita</button>'+'</td>'+'</tr>';
            $('#tbReservacionRealizadas').html(str);
    }
    });
}

function mostrarDetalleCita(i){
    if(i >= 0 && cita !== null && cita[i] !== null){
        
        servicioActual = cita[i];
        
        $('#txtClienteId').val(servicioActual.cliente.idCliente);
        $('#txtMascotaId').val(servicioActual.mascota.idMascota);
        $('#txtCitaId').val(servicioActual.idCita);
        $('#txtReservacionCliente').val(servicioActual.cliente.Persona.nombre +" "+servicioActual.cliente.Persona.apellidoPaterno +" "+servicioActual.cliente.Persona.apellidoMaterno);
        $('#txtReservacionMascota').val(servicioActual.mascota.nombre);
        $('#txtReservacionServicioM').val(servicioActual.servicio.nombreS +"  $"+servicioActual.servicio.precio);
        h = hora(servicioActual.fechaHoraIniAsString);
        f = fecha(servicioActual.fechaHoraIniAsString);
        
        $('#txtReservacionDateM').val(f);
        
        $('#txtReservacionHorarioM').val(h);
     
    }
}

//Formato para la Fecha//
function fecha(fecha){
   a = fecha.substring(-1,4);
   m = fecha.substring(5,7);
   d = fecha.substring(8,10);
   
   return d +"/"+m+"/"+a;
}
//Formato para la hora//
function hora(hora){
   h = hora.substring(11,13);
   m = hora.substring(14,16);
   
   return h +":"+m;
}

function horaF(hora){
   hf = hora.substring(11,13);
   mf = hora.substring(14,16);
   
   return hf +":"+mf;
}
//Cargar clientes en la tabla y colocarlo en el input
function cargarCliente(){
    $.ajax(
            {
                type: "GET",
                async: true,
                url: "api/cliente/getAll"
                //data: busqueda
            }
    ).done(function (data) {
        cliente = data;
        var tabla = '';                
        for (var i = 0; i < data.length; i++) {

            tabla += "<tr>";
            tabla += "<td>" + cliente[i].Persona.nombre +" "+data[i].Persona.apellidoPaterno +" "+data[i].Persona.apellidoMaterno+"</td>";
            tabla += "<td><button class='btn btn-info btn-rounded' onclick='agregarC(" + i + ");'><i class='fas fa-plus-square'></i></button></td>";
            tabla += "</tr>";
        }
        tabla += "</table>";
        $('#tbReservacionC').html(tabla);
    });
}

function agregarC(i) {
    if (i >= 0 && cliente !== null && cliente[i] !== null) {
        clienteActualList = cliente[i];       
        $('#txtReservacionCliente').val(
                clienteActualList.Persona.nombre + " " + 
                clienteActualList.Persona.apellidoPaterno + " " +
                clienteActualList.Persona.apellidoMaterno);       
        $('#txtClienteId').val(clienteActualList.idCliente);      
    }
}

//Cargar mascotas en la tabla y colocarlo en el input
function cargarMascota(){
    var idC = $('#txtClienteId').val(); 
    var idC = {
        idC: idC
    };
    $.ajax(
            {
                type: "GET",
                async: true,
                url: "api/mascota/getAllMCli",
                data: idC
            }
    ).done(function (data) {
        mascotaC = data;
        var tabla = '';                
        for (var i = 0; i < data.length; i++) {

            tabla += "<tr>";
            tabla += "<td>" + mascotaC[i].nombre +"</td>";
            tabla += "<td><button class='btn btn-info btn-rounded' onclick='agregarM(" + i + ");'><i class='fas fa-plus-square'></i></button></td>";
            tabla += "</tr>";
        }
        tabla += "</table>";
        $('#tbReservacionM').html(tabla);
    });
}

function agregarM(i) {
    if (i >= 0 && mascotaC !== null && mascotaC[i] !== null) {
        mascotaActual = mascotaC[i];       
        $('#txtReservacionMascota').val(
                mascotaActual.nombre);       
        $('#txtMascotaId').val(mascotaActual.idMascota);
    }
}
//Cargar Servicios medicos
function cargarServicioM() {
    limpiarServicio();
    $.ajax(
            {
                type: "GET",
                url: "api/servicio/getAllSM",
                async: false
            }).done(
            function (data) {
                servicios = "";
                servicios += "<option>Elija una opcion</option>";
                for (var i = 0; i < data.length; i++) {
                    servicios += "<option value='" + data[i].idServicio + "'  >";
                    servicios += data[i].nombreS+" ";
                    servicios += "           ";
                    servicios += "$"+data[i].precio;
                    servicios += "</option>";
                }
                $('#txtReservacionServicio').html(servicios);
                cargarHorarios();
            }
    );
}
//Cargar Servicios Esteticos
function cargarServicioE() {
    limpiarServicio();
    
    $.ajax(
            {
                type: "GET",
                url: "api/servicio/getAllSE",
                async: false
            }).done(
            function (data) {
                servicios = "";
                servicios += "<option>Elija una opcion</option>";
                for (var i = 0; i < data.length; i++) {
                    servicios += "<option value='" + data[i].idServicio + "'  >";
                    servicios += data[i].nombreS+" ";
                    servicios += "           ";
                    servicios += "$"+data[i].precio;
                    servicios += "</option>";
                }
                $('#txtReservacionServicio').html(servicios);
                cargarHorarios();
            }
    );
}
//Limpiar Servicios
function limpiarServicio() {
    $.ajax(
            {
                async: false
            }).done(
            function (data) {
                $('#txtReservacionServicio').html("");
            }
    );  
}
//Cargar Horarios
function cargarHorarios() {
    $.ajax(
            {
                async: false
            }).done(
            function (data) {
                horarios = "";
                horarios += "<option>Elija una opcion</option>";
                    horarios += "<option value='" + "10:00:00" + "'  >";
                    horarios += "10:00 AM";
                    horarios += "</option>";
                    horarios += "<option value='" + "12:00:00" + "'  >";
                    horarios += "12:00 PM";
                    horarios += "</option>";
                    horarios += "<option value='" + "14:00:00" + "'  >";
                    horarios += "14:00 PM";
                    horarios += "</option>";
                    horarios += "<option value='" + "16:00:00" + "'  >";
                    horarios += "16:00 PM";
                    horarios += "</option>";
                    horarios += "<option value='" + "18:00:00" + "'  >";
                    horarios += "18:00 PM";
                    horarios += "</option>";
                
                $('#txtReservacionHorario').html(horarios);
            }
    );
}
//Hacer Reservacion
function hacerReservacion(){ 
    var mascota = $('#txtMascotaId').val();
    var cliente = $('#txtClienteId').val();
    var servicio = $('#txtReservacionServicio').val();
    var fechaIni = fechaInicio();

    var data = {
        mascota: mascota,
        cliente: cliente,
        servicio: servicio,
        fechaIni: fechaIni
    };  
    console.log(data);
    $.ajax({
        type: "POST",
        async: true,
        url: "api/cita/insert",
        data: data
    }

    ).done(function (data) {
        if (data.result != null) {
            Swal.fire(
                    'Inserción Realizada',
                    " ",
                    'success');
            actualizarTablaServicios();
            limpiarDatosCita();
            return;
        } else {
            Swal.fire("Error de Inserción", " ", "error");
        }
    });
}
//Funcion para la fecha en formato
function fechaInicio(){
   var fecha = $('#txtReservacionDate').val();
   var hora = $('#txtReservacionHorario').val();
   a = fecha.substring(-1,4);
   m = fecha.substring(5,7);
   d = fecha.substring(8,10);
   
   var fechaini= d+"/"+m+"/"+a+" "+hora;
   return fechaini;
   console.log(fechaini);
}
//Limíar Datos
function limpiarDatosCita(){

    $('#txtClienteId').val("");
    $('#txtMascotaId').val("");
    $('#txtReservacionCliente').val("");
    $('#txtReservacionMascota').val("");
    $('#txtReservacionServicio').val("");
    $('#txtReservacionDate').val("");
    $('#txtReservacionHorario').val("");
    $('#txtReservacionServicioM').val("");        
    $('#txtReservacionDateM').val("");      
    $('#txtReservacionHorarioM').val("");
     
}

//Eliminar Cita
function eliminarCita(i) {
    var idCita = i;
    var data = {
        idCita: idCita
    };
    $.ajax({
        type: "POST",
        url: "api/cita/delete",
        data: data,
        async: true
    }).done(function (data) {
        if (data.result != null) {
            Swal.fire(
                    'Modificación Realizada',
                    ":",
                    'success')
            actualizarTablaServicios();

        } else {
            Swal.fire("Error en la eliminación", " ", "error");
            actualizarTablaServicios();

        }
    });
    
}

function actualizarReservacion(){ 
    var servicio = null;
    var mascota = $('#txtMascotaId').val();
    var cliente = $('#txtClienteId').val();
    if($('#txtReservacionServicio').val() != null){
        servicio = $('#txtReservacionServicio').val();
    }else{
        servicio = $('#txtReservacionServicioM').val();
    }
    var fechaIni = fechaInicio();
    var idCita = $('#txtCitaId').val();
    
    alert(fechaIni);
    
    var data = {
        fechaIni: fechaIni,
        mascota: mascota,
        cliente: cliente,
        servicio: servicio,
        idCita:idCita
    };  
    console.log(data);
    $.ajax({
        type: "POST",
        async: true,
        url: "api/cita/updateE",
        data: data
    }

    ).done(function (data) {
        if (data.result != null) {
            Swal.fire(
                    'Actualizacion Realizada',
                    " ",
                    'success');
            actualizarTablaServicios();
            limpiarDatosCita();
            return;
        } else {
            Swal.fire("Error de Inserción", " ", "error");
        }
    });
}

function buscarR() {
    var tableReg = document.getElementById('tbReservacionRealizadas');
    var searchText = document.getElementById('txtBuscar').value.toLowerCase();
    for (var i = 1; i < tableReg.rows.length; i++) {
        var cellsOfRow = tableReg.rows[i].getElementsByTagName('td');
        var found = false;
        for (var j = 0; j < cellsOfRow.length && !found; j++) {
            var compareWith = cellsOfRow[j].innerHTML.toLowerCase();
            if (searchText.length == 0 || (compareWith.indexOf(searchText) > -1)) {
                found = true;
            }
        }
        if (found) {
            tableReg.rows[i].style.display = '';
        } else {
            tableReg.rows[i].style.display = 'none';
        }
    }
}

function terminarCita(i){
    var idCita = i;
    var estatus = 2;
    var data = {
        idCita: idCita,
        est: estatus
    };
    $.ajax({
        type: "POST",
        url: "api/cita/changeEst",
        data: data,
        async: true
    }).done(function (data) {
        if (data.result != null) {
            Swal.fire(
                    'Cita Finalizada Realizada',
                    ":",
                    'success');
            actualizarTablaServicios();

        } else {
            Swal.fire("Error en la eliminación", " ", "error");
            actualizarTablaServicios();

        }
    });
}

function cancelarCita(i){
    var idCita = i;
    var estatus = 0;
    var data = {
        idCita: idCita,
        est: estatus
    };
    $.ajax({
        type: "POST",
        url: "api/cita/changeEst",
        data: data,
        async: true
    }).done(function (data) {
        if (data.result != null) {
            Swal.fire(
                    'Cita Cancelada Realizada',
                    ":",
                    'success');
            actualizarTablaServicios();

        } else {
            Swal.fire("Error en la eliminación", " ", "error");
            actualizarTablaServicios();
        }
    });
    
    actualizarTablaServicios();
}