/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var cliente;
var clienteActual;


//FUNCIONANDO
function cargarModuloCliente() {
    $.ajax(
            {
                type: "GET",
                url: "html/clientes.html",
                async: true
            }
    ).done(function (data)
    {
        actualizarTablaClientes();
        $('#principal').html(data);
    });
}

//FUNCIONANDO
function actualizarTablaClientes() {

    $.ajax(
            {
                type: "GET",
                async: true,
                url: "api/cliente/getAll"
            }
    ).done(function (data)
    {
        cliente = data;
        console.log(cliente);
        var tabla = '';
        for (var i = 0; i < cliente.length; i++) {
            tabla += '<tr>';
            tabla += '<td>' + cliente[i].Persona.nombre + " " + cliente[i].Persona.apellidoPaterno + " "
                    + cliente[i].Persona.apellidoMaterno + '</td>';
            tabla += '<td>' + cliente[i].Persona.genero + '</td>';
            tabla += '<td>' + cliente[i].Persona.calle + "  Col." + cliente[i].Persona.colonia + "  C.P:" + cliente[i].Persona.cp + '</td>';
            tabla += '<td>' + cliente[i].Persona.telefono + '</td>';
            tabla += '<td>' + cliente[i].Usuario.nombreUsuario + '</td>';
            tabla += '<td>' + cliente[i].Usuario.rol + '</td>';
            tabla += '<td>' + cliente[i].correo + '</td>';
            tabla += '<td><a class="btn btn-primary text-white" onclick="mostrarDetalleCliente(' + i + ')" data-toggle="modal" data-target="#detailModalCliente">Detalles</a>' + '</td>';
            tabla += '</tr>';
            $('#tbCliente').html(tabla);
        }

    });
}

//FUNCIONANDO
function mostrarDetalleCliente(posicion) {
    if (posicion >= 0 && cliente !== null && cliente[posicion] !== null) {
        clienteActual = cliente[posicion];

        $('#txtNombre').val(clienteActual.Persona.nombre);
        $('#txtApellidoP').val(clienteActual.Persona.apellidoPaterno);
        $('#txtApellidoM').val(clienteActual.Persona.apellidoMaterno);
        $('#txtGenero').val(clienteActual.Persona.genero);
        $('#txtCalle').val(clienteActual.Persona.calle);
        $('#txtColonia').val(clienteActual.Persona.colonia);
        $('#txtCP').val(clienteActual.Persona.cp);
        $('#txtTelefono').val(clienteActual.Persona.telefono);
        $('#txtUsuario').val(clienteActual.Usuario.nombreUsuario);
        $('#txtContrasena').hide();
        $('#lbCon').hide();
        $('#txtCorreo').val(clienteActual.correo);
        $('#txtClienteId').val(clienteActual.idCliente);
        $('#txtPersonaId').val(clienteActual.Persona.idPersona);
        $('#txtUsuarioId').val(clienteActual.Usuario.idUsuario);

        document.getElementById('btnGuardar').setAttribute('class', 'btn btn-success text-white mr-2');
        document.getElementById('btnGuardar').innerHTML = "Modificar";

        document.getElementById('btnElimCancel').setAttribute('onclick', "eliminarCliente()");
        document.getElementById('btnElimCancel').innerHTML = "Eliminar";

        //console.log(clienteActual);

    }
}

//FUNCIONANDO
function decidirCliente() {
    if (clienteActual != null) {
        modificarCliente();
    } else {
        guardarCliente();
    }
}

//FUNCIONANDO
function modificarCliente() {
    var nombre = $('#txtNombre').val();
    var apeP = $('#txtApellidoP').val();
    var apeM = $('#txtApellidoM').val();
    var gen = $('#txtGenero').val();
    var street = $('#txtCalle').val();
    var col = $('#txtColonia').val();
    var codeCp = $('#txtCP').val();
    var tel = $('#txtTelefono').val();
    var nomU = $('#txtUsuario').val();
    var correo = $("#txtCorreo").val();

    var idCli = $("#txtClienteId").val();
    var idPer = $("#txtPersonaId").val();
    var idUsu = $("#txtUsuarioId").val();

    var data = {
        mail: correo,
        nombreA: nombre,
        apellidoP: apeP,
        apellidoM: apeM,
        genero: gen,
        calle: street,
        colonia: col,
        cp: codeCp,
        telefono: tel,
        nomUsuario: nomU,
        idC: idCli,
        idP: idPer,
        idU: idUsu
    };
    $.ajax({
        type: "POST",
        url: "api/cliente/update",
        data: data,
        async: true
    }).done(function (data) {
        if (data.result != null) {
            Swal.fire("Modificación correcta", " ", "success");
            actualizarTablaClientes();
            limpiarDatosCliente();
            return;
        } else
            Swal.fire("Error de modificación", " ", "error");
    }
    );
}

//FUNCIONANDO
function limpiarDatosCliente() {

    clienteActual = null;

    document.getElementById('btnGuardar').setAttribute('class', 'btn btn-info text-white mr-2');
    document.getElementById('btnGuardar').innerHTML = "Guardar";

    document.getElementById('btnElimCancel').innerHTML = "Cerrar";
    document.getElementById('btnElimCancel').removeAttribute('onclick');

    $('#txtContrasena').show();
    $('#lbCon').show();

    $('#txtNombre').val("");
    $('#txtApellidoP').val("");
    $('#txtApellidoM').val("");
    $('#txtGenero').val("");
    $('#txtCalle').val("");
    $('#txtColonia').val("");
    $('#txtCP').val("");
    $('#txtTelefono').val("");
    $('#txtUsuario').val("");
    $("#txtCorreo").val("");

    $('#txtContrasena').val("");

    $("#txtClienteId").val("");
    $("#txtPersonaId").val("");
    $("#txtUsuarioId").val("");
}

//FUNCIONANDO
function eliminarCliente() {
    var id = $('#txtClienteId').val();
    var data = {
        idC: id
    };
    $.ajax({
        type: "POST",
        url: "api/cliente/delete",
        data: data,
        async: true
    }).done(function (data) {
        if (data.result != null) {
            Swal.fire("Eliminación correcta", " ", "success");
            actualizarTablaClientes();
            
        } else
            Swal.fire("Error de eliminación", " ", "error");
    });
}

//FUNCIONANDO
function guardarCliente() {
    var nom = $('#txtNombre').val();
    var apeP = $('#txtApellidoP').val();
    var apeM = $('#txtApellidoM').val();
    var gen = $('#txtGenero').val();
    var street = $('#txtCalle').val();
    var col = $('#txtColonia').val();
    var codeCp = $('#txtCP').val();
    var tel = $('#txtTelefono').val();
    var nomU = $('#txtUsuario').val();
    var pass = $('#txtContrasena').val();
    var correo = $("#txtCorreo").val();

    var data = {
        mail: correo,
        nombre: nom,
        apellidoP: apeP,
        apellidoM: apeM,
        genero: gen,
        calle: street,
        colonia: col,
        cp: codeCp,
        telefono: tel,
        nomUsuario: nomU,
        password: pass
    };

    $.ajax({
        type: "POST",
        url: "api/cliente/insert",
        data: data,
        async: true
    }).done(function (data) {
        if (data.result != null) {
            Swal.fire("Inserción Realizada", " ", "success");
            actualizarTablaClientes();
            limpiarDatosCliente();
            return;
        } else
            Swal.fire("Error de Inserción", " ", "error");
        limpiarDatosCliente();
    });
}

function buscarCliente() {
    var tableReg = document.getElementById('tbCliente');
    var searchText = document.getElementById('txtBuscarCliente').value.toLowerCase();
    for (var i = 1; i < tableReg.rows.length; i++) {
        var cellsOfRow = tableReg.rows[i].getElementsByTagName('td');
        var found = false;
        for (var j = 0; j < cellsOfRow.length && !found; j++) {
            var compareWith = cellsOfRow[j].innerHTML.toLowerCase();
            if (searchText.length == 0 || (compareWith.indexOf(searchText) > -1)) {
                found = true;
            }
        }
        if (found) {
            tableReg.rows[i].style.display = '';
        } else {
            tableReg.rows[i].style.display = 'none';
        }
    }
}