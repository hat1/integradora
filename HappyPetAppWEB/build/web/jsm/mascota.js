/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var mascota;
var cliente; //Variable para guardar el cliente en las listas
var clienteActualList; //Variable para guardar el cliente seleccionado
var mascotaActual;

//FUNCIONANDO
function cargarModuloMascota(){
    $.ajax(
            {
                type: "GET",
                url: "html/mascotas.html",
                async: true
            }
    ).done(function (data) {
        $('#principal').html(data);
        actualizarTablaMascota();
    });
}

//FUNCIONANDO
function actualizarTablaMascota(){
    $.ajax(
            {
                type: "GET",
                async: true,
                url: "api/mascota/getAllM"
            }
    ).done(function (data)
    {
        mascota = data;
        var str = '';
        for (var i = 0; i < mascota.length; i++) {
            str += '<tr>' +
                    '<td>' + mascota[i].nombre + '</td>' +
                    '<td>' + mascota[i].edad + '</td>' +
                    '<td>' + mascota[i].raza + '</td>' +
                    '<td>' + mascota[i].especie + '</td>' +
                    '<td>' + mascota[i].sexo + '</td>' +
                    '<td>' + mascota[i].descripcion + '</td>' +
                    '<td><img width="70" height="70" id="imgFoto" src="data:;base64,' + mascota[i].foto + '"/></td>' +
                    '<td>' + mascota[i].cliente.Persona.nombre + " " + data[i].cliente.Persona.apellidoPaterno + " " + data[i].cliente.Persona.apellidoMaterno + '</td>' +
                    '<td><a class="btn btn-primary text-white" onclick="mostrarDetalleMascota(' + i + ')" data-toggle="modal" data-target="#detailModalMascota">Detalles</a>' + '</td>' +
                    '</tr>';
            $('#tbMascotas').html(str);
        }
    });
}

//FUNCIONANDO
function mostrarDetalleMascota(posicion){
    if(posicion >= 0 && mascota !== null && mascota[posicion] !== null){
        
        mascotaActual = mascota[posicion];
        
        $('#tabla').remove();
        
        $('#txtNombre').val(mascotaActual.nombre);
        $('#txtEdad').val(mascotaActual.edad);
        $('#txtRaza').val(mascotaActual.raza);
        $('#txtEspecie').val(mascotaActual.especie);
        $('#txtGenero').val(mascotaActual.sexo);
        $('#txtDescripcion').val(mascotaActual.descripcion);
        
        document.getElementById("imgFoto2").setAttribute("src", "data:;base64," + mascotaActual.foto);
        
        $('#clienteSelect').val(
                mascotaActual.cliente.Persona.nombre + " " + 
                mascotaActual.cliente.Persona.apellidoPaterno + " " + 
                mascotaActual.cliente.Persona.apellidoMaterno);
        
        $('#txtClienteId').val(mascotaActual.cliente.idCliente);
        $('#txtMascotaId').val(mascotaActual.idMascota);
        
        
        document.getElementById('btnGuardar').setAttribute('class', 'btn btn-success text-white mr-2');
        document.getElementById('btnGuardar').innerHTML = "Modificar";

        document.getElementById('btnElimCancel').setAttribute('onclick', "eliminarMascota()");
        document.getElementById('btnElimCancel').innerHTML = "Eliminar";
    }
}

function cargarFoto1() {
    var fileChooser = document.getElementById("txtFoto2");
    var foto = document.getElementById("imgFoto2");
    var base64 = document.getElementById("txtBase642");
    if (fileChooser.files.length > 0) {
        var fr = new FileReader();
        fr.onload = function () {
            foto.src = fr.result;
            base64.value = foto.src.replace
                    (/^data:image\/(png|jpg|jpeg|gif);base64,/, '');
        };
        fr.readAsDataURL(fileChooser.files[0]);
    }
}

//FUNCIONANDO
function cargarClienteTabla(){

    console.log("asdasdasd");

    $.ajax(
            {
                type: "GET",
                async: true,
                url: "api/cliente/getAll"
            }
    ).done(function (data) {
        cliente = data;
        console.log(cliente);
        var tabla = '';
                tabla += "<table class='table table-hover table-border' id='tabla'>";
                tabla += "<thead class='bg-color-hp text-white'>";
                tabla += "<tr>\n\
                        \n\<th class='th-sm text-center'>Nombre</th>\n\
                        \n\<th class='th-sm text-center'>Apellido Paterno</th>\n\
                        \n\<th class='th-sm text-center'>Apellido Materno</th>\n\
                        \n\<th class='th-sm text-center'>Agregar</th>\n\
                        </tr>\n\
                </thead>";
        for (var i = 0; i < data.length; i++) {

            tabla += "<tr>";
            tabla += "<td>" + cliente[i].Persona.nombre + "</td>";
            tabla += "<td>" + cliente[i].Persona.apellidoPaterno + "</td>";
            tabla += "<td>" + cliente[i].Persona.apellidoMaterno + "</td>";
            tabla += "<td><button class='btn btn-info btn-rounded' onclick='agregarC(" + i + ");'><i class='fas fa-plus-square'></i></button></td>";
            tabla += "</tr>";
        }
        tabla += "</table>";

        $('#tbCliente').html(tabla);
    });
}

//FUNCIONANDO
function agregarC(i) {
    //var nombre = personas[i].idPersona.nombre + " " + personas[i].idPersona.apellidoPaterno + " " +personas[i].idPersona.apellidoMaterno;
    
    
    
    if (i >= 0 && cliente !== null && cliente[i] !== null) {
        clienteActualList = cliente[i];
        
        $('#clienteSelect').val(
                clienteActualList.Persona.nombre + " " + 
                clienteActualList.Persona.apellidoPaterno + " " +
                clienteActualList.Persona.apellidoMaterno);
        
        $('#txtClienteId').val(clienteActualList.idCliente);
        
//        $('#clienteSelect').val(mascotaActual.Persona.nombre + " " + mascotaActual.Persona.apellidoPaterno + " " + mascotaActual.Persona.apellidoMaterno);
//        $('#txtClienteId').val(mascotaActual.idCliente);
    }
}

//FUNCIONANDO
function modificarMascota() {
    var idMascota = $('#txtMascotaId').val();
    var nombre = $('#txtNombre').val();
    var edad = $('#txtEdad').val();
    var raza = $('#txtRaza').val();
    var especie = $('#txtEspecie').val();
    var sexo = $('#txtGenero').val();
    var descripcion = $('#txtDescripcion').val();
    var image = $("#txtBase642").val();
    var cliente = $('#txtClienteId').val();

    var mascotas = {
        idMascota: idMascota,
        nombre: nombre,
        edad: edad,
        raza: raza,
        especie: especie,
        sexo: sexo,
        descripcion: descripcion,
        foto: image,
        cliente: cliente
    };

    $.ajax({
        type: "POST",
        url: "api/mascota/update",
        data: mascotas,
        async: true
    }).done(function (data) {
        if (data.result !== null) {
            Swal.fire(
                    'Modificación Realizada',
                    " ",
                    'success');
            actualizarTablaMascota();
            limpiarDatosMascotas();
            return;
        } else {
            Swal.fire("Error en la modificación", " ", "error");
        }
    });
}

//FUNCIONANDO
function decidirMascota(){
    if(mascotaActual != null){
        modificarMascota();
        //console.log("modif");
    }else{
        guardarMascota();
        //console.log("guard");
    }
}

//FUNCIONANDO
function limpiarDatosMascotas(){
    
    mascotaActual = null;
    clienteActualList = null;
    
    
    document.getElementById('btnGuardar').setAttribute('class', 'btn btn-info text-white mr-2');
    document.getElementById('btnGuardar').innerHTML = "Guardar";

    document.getElementById('btnElimCancel').innerHTML = "Cerrar";
    document.getElementById('btnElimCancel').removeAttribute('onclick');
    
    
    $('#tabla').remove();
    $('#txtNombre').val("");
    $('#txtEdad').val("");
    $('#txtRaza').val("");
    $('#txtEspecie').val("");
    $('#txtGenero').val("");
    $('#txtDescripcion').val("");
    $('#clienteSelect').val("");
    document.getElementById("imgFoto2").setAttribute("src", "");
    
}

//FUNCIONANDO
function guardarMascota(){
    
    var nombre = $('#txtNombre').val();
    var edad = $('#txtEdad').val();
    var raza = $('#txtRaza').val();
    var especie = $('#txtEspecie').val();
    var sexo = document.getElementById('txtGenero').value;
    var descripcion = $('#txtDescripcion').val();
    var img = $("#txtBase642").val();
    var cliente = $('#txtClienteId').val();

    var data = {
        nombre: nombre,
        edad: edad,
        raza: raza,
        especie: especie,
        sexo: sexo,
        descripcion: descripcion,
        foto: img,
        cliente: cliente
    };
    
    
    console.log(data);

    $.ajax({
        type: "POST",
        asyn: true,
        url: "api/mascota/insert",
        data: data
    }

    ).done(function (data) {
        if (data.result != null) {
            Swal.fire(
                    'Inserción Realizada',
                    " ",
                    'success');
            actualizarTablaMascota();
            limpiarDatosMascotas();
            return;
        } else {
            Swal.fire("Error de Inserción", " ", "error");
        }
    });
}

//FUNCIONANDO
function eliminarMascota() {
    var idMascota = $('#txtMascotaId').val();
    var data = {
        idMascota: idMascota
    };
    $.ajax({
        type: "POST",
        url: "api/mascota/delete",
        data: data,
        async: true
    }).done(function (data) {

        if (data.result != null) {
            Swal.fire(
                    'Eliminación Realizada',
                    ":",
                    'success')
            actualizarTablaMascota();
        } else {
            Swal.fire("Error en la eliminación", " ", "error");
        }
    });
}

function buscarMascota() {
    var tableReg = document.getElementById('tbMascotas');
    var searchText = document.getElementById('txtBuscarMascotas').value.toLowerCase();
    for (var i = 1; i < tableReg.rows.length; i++) {
        var cellsOfRow = tableReg.rows[i].getElementsByTagName('td');
        var found = false;
        for (var j = 0; j < cellsOfRow.length && !found; j++) {
            var compareWith = cellsOfRow[j].innerHTML.toLowerCase();
            if (searchText.length == 0 || (compareWith.indexOf(searchText) > -1)) {
                found = true;
            }
        }
        if (found) {
            tableReg.rows[i].style.display = '';
        } else {
            tableReg.rows[i].style.display = 'none';
        }
    }
}
