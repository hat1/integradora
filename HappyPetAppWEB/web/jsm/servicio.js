/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var servicios;
var servicioActual;

//FUNCIONANDO
function cargarModuloServicios(){
    $.ajax(
            {
                type: "GET",
                url: "html/servicio.html",
                async: true
            }
    ).done(function (data) {
        $('#principal').html(data);
        actualizarTablaServicio();
    });
}

//FUNCIONANDO
function actualizarTablaServicio() {

    $.ajax(
            {
                type: "GET",
                async: true,
                url: "api/servicio/getAll"
            }
    ).done(function (data)
    {


        servicios = data;
        console.log(data);
        var str = '';
        var tipo = '';
        for (var i = 0; i < servicios.length; i++) {
            if (servicios[i].tipos === 1) {
                tipo = 'Medico';
            } else {
                tipo = 'Estetico';
            }

            str += '<tr>' +
                    '<td class="th-sm h6 ">' + servicios[i].nombreS + '</td>' +
                    '<td class="th-sm h6 ">' + servicios[i].descrip + '</td>' +
                    '<td class="th-sm h6 ">' + "$" + servicios[i].precio + '</td>' +
                    '<td class="th-sm h6 ">' + tipo + '</td>' +
                    '<td><a class="btn btn-primary text-white" onclick="mostrarDetalleServicio(' + i + ')" data-toggle="modal" data-target="#detailModal">Detalles</a>' + '</td>' +
                    '</td>' +
                    '</tr>';

            $('#tbServicio').html(str);
        }


    });

}

//FUNCIONANDO
function mostrarDetalleServicio(posicion){
    if (posicion >= 0 && servicios !== null && servicios[posicion] !== null) {
        //changeButtonColorName();
        servicioActual = servicios[posicion];

        $('#txtNombre').val(servicioActual.nombreS);
        $('#txtDescripcion').val(servicioActual.descrip);
        $('#txtPrecio').val(servicioActual.precio);
        if (servicioActual.tipos === 1) {
            document.getElementById("txtTipo1").checked = true;
        } else {
            document.getElementById("txtTipo2").checked = true;
        }

        $('#txtIdServicio').val(servicioActual.idServicio);
        
        document.getElementById('btnGuardar').setAttribute('class', 'btn btn-success text-white mr-2');
        document.getElementById('btnGuardar').innerHTML = "Modificar";
        
        document.getElementById('btnElimCancel').setAttribute('onclick', "eliminarServicio()");
        document.getElementById('btnElimCancel').innerHTML = "Eliminar";
    }
    
    
}

//FUNCIONANDO
function decidirServicio(){
    if(servicioActual != null){
        modificarServicio();
    }else{
        guardarServicio();
    }
}

//FUNCIONANDO
function modificarServicio(){
    var nombre = $('#txtNombre').val();
    var desc = $('#txtDescripcion').val();
    var pres = $('#txtPrecio').val();
    var tip = 0;

    //Obtencion del tipo de servicio utilizando el radiobutton
    if (document.getElementById("txtTipo1").checked) {
        tip = 1;
    } else {
        tip = 2;
    }

    var idServicio = $("#txtIdServicio").val();

    var data = {

        nomS: nombre,
        descS: desc,
        precioS: pres,
        tipoS: tip,
        idS: idServicio
    };

    $.ajax({
        type: "POST",
        url: "api/servicio/update",
        data: data,
        async: true
    }).done(function (data) {
        if (data.result != null) {
            Swal.fire("Modificación correcta", " ", "success");
            
            actualizarTablaServicio();
            limpiarDatosServicios();
            return;
            
        } else {
            limpiarDatosServicios();
            Swal.fire("Error de modificación", " ", "error");
            return;
        }
    });
    
    
}

//FUNCIONANDO
function limpiarDatosServicios() {
    
    servicioActual = null;
    
    document.getElementById('btnGuardar').setAttribute('class', 'btn btn-info text-white mr-2');
    document.getElementById('btnGuardar').innerHTML = "Guardar";
    
    document.getElementById('btnElimCancel').innerHTML = "Cerrar";
    document.getElementById('btnElimCancel').removeAttribute('onclick');
    
    $('#txtNombre').val("");
    $('#txtDescripcion').val("");
    $('#txtPrecio').val("");
    document.getElementById("txtTipo1").checked = false;
    document.getElementById("txtTipo2").checked = false;
}

//FUNCIONANDO
function guardarServicio() {
    
    //var token = localStorage.getItem("token");
    var nombre = $('#txtNombre').val();
    var desc = $('#txtDescripcion').val();
    var pres = $('#txtPrecio').val();
    var tip = 0;

    //Obtencion del tipo de servicio utilizando el radiobutton
    if (document.getElementById("txtTipo1").checked) {
        tip = 1;
    } else {
        tip = 2;
    }

    var data = {

        nomS: nombre,
        descS: desc,
        precioS: pres,
        tipoS: tip
    };

    $.ajax({
        type: "POST",
        url: "api/servicio/insert",
        data: data,
        async: true
    }).done(function (data) {
        if (data.result != null) {

            Swal.fire("Inserción Realizada", " ", "success");
            
            actualizarTablaServicio();
            limpiarDatosServicios();
            return;

        } else {
            Swal.fire("Error de Inserción", " ", "error");
            limpiarDatosServicios();
          
        }
    });
}

function eliminarServicio(){
    var idSer = $("#txtIdServicio").val();
    
    var data = {
        idS: idSer
    };
    
    $.ajax({
        type: "POST",
        url: "api/servicio/delete",
        data: data,
        async: true
    }).done(function (data) {
        if (data.result != null){
            Swal.fire("Eliminacion correcta", " ", "success");
            actualizarTablaServicio();
        }else{
            Swal.fire("Error de Eliminacion", " ", "error");
        }
    });
}

function buscarSer() {
    var tableReg = document.getElementById('tbServicio');
    var searchText = document.getElementById('txtBuscar').value.toLowerCase();
    for (var i = 1; i < tableReg.rows.length; i++) {
        var cellsOfRow = tableReg.rows[i].getElementsByTagName('td');
        var found = false;
        for (var j = 0; j < cellsOfRow.length && !found; j++) {
            var compareWith = cellsOfRow[j].innerHTML.toLowerCase();
            if (searchText.length == 0 || (compareWith.indexOf(searchText) > -1)) {
                found = true;
            }
        }
        if (found) {
            tableReg.rows[i].style.display = '';
        } else {
            tableReg.rows[i].style.display = 'none';
        }
    }
}