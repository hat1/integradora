//function validar() {
//    var usuario = document.getElementById("txtUsuario").value;
//    var contraseña = document.getElementById("txtClave").value;
//
//    if (usuario === "Rober" && contraseña === "1234" || usuario === "Jonny" && contraseña === "5678" || usuario === "Hector" && contraseña === "8901") {
//        Swal.fire({
//            position: 'top-center',
//            icon: 'success',
//            title: "¡Bienvenido!",
//            text: "Usuario y contraseña validos",
//            showConfirmButton: false,
//            timer: 1500
//        });
//        setTimeout("cambioDePagina()", 1500);
//
//    } else {
//        Swal.fire({
//            position: 'top-center',
//            icon: 'error',
//            title: "¡Hubo un error!",
//            text: "Verifique sus datos",
//            showConfirmButton: false,
//            timer: 1500
//        });
//    }
//}


function validar(){
    var usuario = $('#txtUsuario').val();
    var contraseña = $('#txtClave').val();
    var data = {
        userN: usuario,
        passU: contraseña
    };
    
    $.ajax({
        type: "POST",
        url: "api/login/verificar",
        data: data,
        async: true
    }).done(function (data){
        if(data.result != null){
            if(data.result == 0){
                Swal.fire({
                    position: 'top-center',
                    icon: 'error',
                    title: "¡Usuario no encontrado!",
                    text: "Intentelo nuevamente",
                    showConfirmButton: false,
                    timer: 1500
                });
                
                
            }else{
                Swal.fire({
                    position: 'top-center',
                    icon: 'success',
                    title: "¡Bienvenido!",
                    text: "Usuario y contraseña validos",
                    showConfirmButton: false,
                    timer: 1500
                });
                setTimeout("cambioDePagina()", 1500);
            }
        }
        
        if(data.error != null){
            Swal.fire({
                    position: 'top-center',
                    icon: 'error',
                    title: "¡Ha ocurrido un error!",
                    text: "Intentelo nuevamente",
                    showConfirmButton: false,
                    timer: 1500
                });
        }
    });
}

function cambioDePagina() {
    location.href = "principal.html";
}

function cerrarSesion() {
    Swal.fire({
        position: 'top-center',
        icon: 'success',
        title: "¡Cerrando sesion!",
        text: "Vuelva pronto",
        showConfirmButton: false,
        timer: 1500
    });
    setTimeout("cambioDePagina1()", 1500);
}

function cambioDePagina1() {
    location.href = "index.html";
}

/*function  logueo(){
 
var u=document.getElementById("username").value;
var p=document.getElementById("pass").value;


var data={
    u:u,
    p:p
};
//token : localStorage.token;
$.ajax(
        {
            type:"POST",
            url:"api/logueo/validar",
            data:data
        }).done(function(data){
            console.log(data);
           if(data == "Usuario no encontrado"){
            Swal.fire("Usuario Incorrecto", " ", "error");
           }else{
              localStorage.token = ("Token",data);
              
            $(location).attr('href',"http://localhost:8084/VeterinariaFinal/index1.html");

           }
    });

}

function  logueoEmpleado(){
 
var u=document.getElementById("username").value;
var p=document.getElementById("pass").value;


var data={
    u:u,
    p:p
};
//token : localStorage.token;
$.ajax(
        {
            type:"POST",
            url:"api/logueo/validarE",
            data:data
        }).done(function(data){
            console.log(data);
           if(data == "Usuario no encontrado"){
            Swal.fire("Usuario Incorrecto", " ", "error");
           }else{
              localStorage.token = ("Token",data);
            $(location).attr('href',"http://localhost:8084/VeterinariaFinal/index_1.html");

           }
    });

}

function desvalidar(){
               localStorage.token = "";
               $(location).attr('href',"index.html");
           }
*/