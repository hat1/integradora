/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var producto;
var productoActual;

//FUNCIONANDO
function cargarModuloProducto() {
    $.ajax(
            {
                type: "GET",
                url: "html/productos.html",
                async: true
            }
    ).done(function (data)
    {
        $('#principal').html(data);
        actualizarTablaProducto();
    });
}

//FUNCIONANDO
function actualizarTablaProducto() {
    $.ajax(
            {
                type: "GET",
                async: true,
                url: "api/producto/getAll"
            }
    ).done(function (data)
    {
        producto = data;
        console.log(producto);
        var tabla;
        for (var i = 0; i < producto.length; i++) {
            tabla += '<tr>';
            tabla += '<td>' + producto[i].nombrepr + '</td>';
            tabla += '<td>' + producto[i].descripccion + '</td>';
            tabla += '<td>' + "$" + producto[i].precio + '</td>';
            tabla += '<td>' + producto[i].proveedor + '</td>';
            tabla += '<td><img width="70" height="70" id="imgFoto" src="data:;base64,' + producto[i].foto + '"/></td>';
            tabla += '<td><a class="btn btn-primary text-white" onclick="mostrarDetalleProducto(' + i + ')" data-toggle="modal" data-target="#detailModal">Detalles</a>' + '</td>';
            tabla += '</tr>';
            $('#tbProducto').html(tabla);
        }


    });
}

//FUNCIONANDO
function mostrarDetalleProducto(posicion) {
    if (posicion >= 0 && producto != null && producto[posicion] != null) {

        productoActual = producto[posicion];

        $('#txtNombre').val(productoActual.nombrepr);
        $('#txtDescripccion').val(productoActual.descripccion);
        $('#txtprecio').val(productoActual.precio);
        $('#txtproveedor').val(productoActual.proveedor);

        document.getElementById("imgFoto2").setAttribute("src", "data:;base64," + productoActual.foto);

        $('#txtIdProducto').val(productoActual.idProducto);

        document.getElementById('btnGuardar').setAttribute('class', 'btn btn-success text-white mr-2');
        document.getElementById('btnGuardar').innerHTML = "Modificar";
        
        
        document.getElementById('btnElimCancel').setAttribute('onclick', "eliminarProducto()");
        document.getElementById('btnElimCancel').innerHTML = "Eliminar";
        
    }

}

//FUNCIONANDO
function cargarFoto1() {
    var fileChooser = document.getElementById("txtFoto2");
    var foto = document.getElementById("imgFoto2");
    var base64 = document.getElementById("txtBase642");
    if (fileChooser.files.length > 0) {
        var fr = new FileReader();
        fr.onload = function () {
            foto.src = fr.result;
            base64.value = foto.src.replace
                    (/^data:image\/(png|jpg|jpeg|gif);base64,/, '');
        };
        fr.readAsDataURL(fileChooser.files[0]);
    }
}

//FUNCIONANDO
function modificarProducto() {
    var nombre = $("#txtNombre").val();
    var descrip = $("#txtDescripccion").val();
    var pre = $("#txtprecio").val();
    var prov = $("#txtproveedor").val();
    var image = $("#txtBase642").val();
    var idProduc = $('#txtIdProducto').val();

    console.log(idProduc);

    var data = {
        idPro: idProduc,
        nombrepr: nombre,
        descripccion: descrip,
        precio: pre,
        proveedor: prov,
        fotoPr: image
    };

    $.ajax({
        type: "POST",
        url: "api/producto/update",
        data: data,
        async: true
    }).done(function (data) {
        if (data.result != null) {
            Swal.fire("Modificación correcta", " ", "success");
            actualizarTablaProducto();
        } else {
            Swal.fire("Error de modificación", " ", "error");
        }
    });
}

//FUNCIONANDO
function guardarProducto() {
    var nombre = $("#txtNombre").val();
    var descrip = $("#txtDescripccion").val();
    var pre = $("#txtprecio").val();
    var prov = $("#txtproveedor").val();
    var image = $("#txtBase642").val();
    
    var data = {
        nombrepr: nombre,
        descripccion: descrip,
        precio: pre,
        proveedor: prov,
        foto: image
    };
    
    $.ajax({
        type: "POST",
        url: "api/producto/insert",
        data: data,
        async: true
    }).done(function (data) {
        
        console.log(data.result);
        if (data.result != null) {
            Swal.fire("Inserción Realizada", " ", "success");
            limpiarDatosProductos();
            actualizarTablaProducto();
            return;
        } else
            
            Swal.fire("Error de Inserción", " ", "error");
            limpiarDatosProductos();
    });
}

//FUNCIONANDO
function decidirProducto() {
    if (productoActual != null) {
        modificarProducto();
    } else {
        guardarProducto();
    }
}

//FUNCIONANDO
function limpiarDatosProductos() {
    productoActual = null;
    
    document.getElementById('btnGuardar').setAttribute('class', 'btn btn-info text-white mr-2');
    document.getElementById('btnGuardar').innerHTML = "Guardar";
    
    document.getElementById('btnElimCancel').innerHTML = "Cerrar";
    document.getElementById('btnElimCancel').removeAttribute('onclick');
    

    $("#txtNombre").val("");
    $("#txtDescripccion").val("");
    $("#txtprecio").val("");
    $("#txtproveedor").val("");
    $("#txtBase642").val("");
    $('#txtIdProducto').val("");
    document.getElementById("imgFoto2").setAttribute("src", "");
}

//FUNCIONANDO
function eliminarProducto(){
    var idProducto = $('#txtIdProducto').val();
    var data = {
        idPr: idProducto
    };
    $.ajax({
        type: "POST",
        url: "api/producto/delete",
        data: data,
        async: true
    }).done(function (data) {
        if (data.result != null) {
            Swal.fire("Eliminación correcta", " ", "success");
            actualizarTablaProducto();
        } else {
            Swal.fire("Error de modificación", " ", "error");
        }
    });
}

function buscarProducto() {
    var tableReg = document.getElementById('tbProducto');
    var searchText = document.getElementById('txtBuscarProducto').value.toLowerCase();
    for (var i = 1; i < tableReg.rows.length; i++) {
        var cellsOfRow = tableReg.rows[i].getElementsByTagName('td');
        var found = false;
        for (var j = 0; j < cellsOfRow.length && !found; j++) {
            var compareWith = cellsOfRow[j].innerHTML.toLowerCase();
            if (searchText.length == 0 || (compareWith.indexOf(searchText) > -1)) {
                found = true;
            }
        }
        if (found) {
            tableReg.rows[i].style.display = '';
        } else {
            tableReg.rows[i].style.display = 'none';
        }
    }
}

